---
title: Documentation du client
description: 
published: true
date: 2024-11-06T07:04:35.362Z
tags: 
editor: markdown
dateCreated: 2024-11-06T07:04:35.362Z
---

# Documentation du client web

La documentation du client web est disponible sur https://optimus.demoptimus5.ovh/optimus-devtools/client-doc

N'hésitez pas à venir échanger avec les développeurs du projet sur https://discord.cybertron.fr