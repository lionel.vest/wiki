---
title: Vérifier sa réputation mail
description: 
published: true
date: 2025-02-14T04:02:45.616Z
tags: 
editor: markdown
dateCreated: 2023-02-05T20:47:49.426Z
---

# Vérifier votre réputation mail pour passer les antispams

OPTIMUS intègr un serveur mail.

Cela signifie que vous hébergez vous même vos emails et que vous les envoyez à partir de votre propre serveur.

Vous luttez ainsi contre des mastodontes comme GMAIL, OUTLOOK ou HOTMAIL, qui n'aiment pas qu'on les concurrence sur ce marché.

Ces GAFAMs ne vont pas hésiter à user de leur position dominante en catégorisant vos emails en SPAM auprès de leurs utilisateurs, dès lors que le paramétrage de votre serveur ne leur convient pas.

Notre système intègre toutes les parades possibles pour vous garantir une réputation maximale auprès de ces opérateurs.


---


Vous pouvez tester la réputation de vos mails en utilisant le service 
https://mail-tester.com
Il est gratuit jusqu'à 5 essais par jour.

Il vous suffit d'envoyer un email à l'adresse indiquée et de consulter le rapport de résultat.

Depuis le webmail intégré à nos serveurs ALLSAPRK, vous devriez obtenir un score de 10/10

![mailtester-1010.png](/mailtester-1010.png)

Si vous obtenez des notes inférieures à 8/10 c'est qu'il y a un mauvais paramétrage de votre serveur.

> Vous pouvez perdre des points si vous envoyez un courriel de test vide ou contenant trop peu de texte. Nous vous recommandons d'intégrer un texte long, par exemple un extrait de page wikipedia.
{.is-warning}

---

> Une mauvaise note peut provenir d'un mauvais paramétrage de votre logiciel d'envoi. Si vous utilisez Outlook, Thunderbird ou un autre client comme celui intégré à votre smartphone, veillez à ce que votre serveur d'envoi soit toujours votre serveur OPTIMUS et pas le serveur de votre fournisseur d'accès.
{.is-info}

> Une signature mail mal configurée ou contenant des images externes peut aussi vous faire perdre de points. Il est préférable de générer vos signatures avec le service : https://www.mail-signatures.com/signature-generator/
{.is-info}

> Notez qu'une note de 10/10 n'exclut pas que le destinataire vous ait manuellement placé dans sa liste noire. Dans ce cas, il faut prendre contact avec le destinataire pour qu'il vous mette en liste blanche.
{.is-info}

> Si votre serveur est hébergé chez OVH, il arrive parfois qu'à cause d'un autre client OVH, tout un groupe d'adresses IP OVH soient considérées comme suspectes par l'organisme UCEPROTECTL3. En résumé : à cause d'un mauvais élève, toute la classe est punie. Dans ce cas, vous pouvez malheureusement subir un déclassement momentané. Celui-ci dure rarement plus de 6 à 7 jours et il n'y a rien que vous puissiez faire de votre côté.
{.is-info}

> Si vous envoyez des mails avec plus de 20 destinataires en copie, il est possible que certains systèmes vous mettent en liste noire.
{.is-info}

> Si vous envoyez une lettre de diffusion à une centaine de personnes, il suffit parfois qu'une seule personne vous signale comme SPAM pour que votre mail soit considéré comme spam par les 99 autres. Pour envoyer des newsletters sur de larges listes de diffusion, il vaut mieux privilégier des services comme BREVO.
{.is-info}

> Un mauvais paramétrage de votre nom de domaine peut aussi créer des perturbations. Notamment, si vous disposez de votre propre site internet qui fonctionne avec le même domaine que celui avec lequel vous envoyez vos emails. Assurez vous d'utiliser le service optimus-website pour paramétrer la redirection vers votre site internet.
{.is-info}

> Google pénalise les adresses mails créées sur un nom de domaine qui a été créé il y a moins de 28 jours. Il est donc préférable d'anticiper en réservant votre nom de domaine 1 mois avant l'utilisation d'optimus-mail.
{.is-info}




