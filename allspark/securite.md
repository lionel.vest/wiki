---
title: Sécurité
description: Sécurité
published: true
date: 2024-11-06T06:19:47.859Z
tags: 
editor: markdown
dateCreated: 2021-06-07T15:45:49.998Z
---

# Chiffrement
OPTIMUS intègre une solution de chiffrement (cryptage) des disques utilisant une clé inviolable de niveau militaire.

Ainsi, si votre serveur est volé ou redémarré, personne ne pourra accéder à vos données sans votre accord préalable. De même, votre serveur peut être verrouillé en urgence par simple redémarrage à distance.

> A l'issue du processus d'installation, veillez à bien sauvegarder votre clé de chiffrement et à la conserver dans un endroit sûr. CYBERTRON n'en détient aucune copie. Si vous la perdez, personne ne pourra récupérer vos données.
{.is-danger}

# Authentification à deux facteurs
Pour assurer un niveau maximal de sécurité, l'authentification sur la console d'administration SSH intègre un code 2FA (2 facteurs). Il s'agit d'un code complémentaire à 6 chiffres qui change toutes les 30 secondes. Vous pouvez générer ce code grâce à votre smartphone en installant par exemple le logiciel gratuit 2FAS.

> La version Android est téléchargeable [ici](https://play.google.com/store/apps/details?id=com.twofasapp)
> 
> La version pour IOS (Iphone) est téléchargeable [ici](https://itunes.apple.com/us/app/2fas-auth/id1217793794?mt=8)
{.is-info}

Pour synchroniser 2FAS avec votre serveur, il suffit de cliquer sur l'icone + en bas à droite de l'application. Deux options s'offrent ensuite à vous :
- recopier manuellement la clé de 16 caractères qui vous est donnée lors de l'installation (laborieux)
- scanner le QRCODE qui apparait à l'écran pendant l'installation avec votre smartphone (instantané)

Ce système garantit que personne ne puisse accéder à votre serveur sans détenir, à la fois votre mot de passe administrateur, et votre smartphone, ce qui rend une attaque hautement improbable.


# Sécurisation
OPTIMUS met en oeuvre des mesures de sécurité draconniennes :
- installation du pare-feu UFW et ouverture des seuls ports requis au fonctionnement du serveur.
- changement du port SSH par défaut (22) pour le port 7822 pour réduire la surface des attaques.
- mise en place d'une authentification à deux facteurs pour le login SSH
- installation (optionnelle) d'une séquence de port knocking pour ouvrir le port SSH (par défaut les ports TCP 1083 1080 1082 1075), ce qui revient à ajouter un 3e facteur d'authentification caché.
- modification du mot de passe des utilisateurs par défaut "root" et "debian" pour des chaines sécurisées de 32 caractères aléatoires
- installation de FAIL2BAN pour bannir les adresses IP qui tentent d'attaquer le serveur après 8 tentatives de mot de passe infructueuses.
- désactivation du login SSH pour l'utilisateur root.

Ces mesures sont conformes aux [recommandations de l'Agence Nationale de Sécurité des Systèmes d'Information (ANSSI)](https://www.ssi.gouv.fr/uploads/2018/04/anssi-guide-admin_securisee_si_v3-0.pdf) et vont même au delà.

