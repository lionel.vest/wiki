---
title: Agrandir son disque
description: Agrandir son disque sur un VPS
published: true
date: 2024-12-20T08:07:08.477Z
tags: 
editor: markdown
dateCreated: 2022-06-09T20:52:02.059Z
---

# Agrandir son disque sur un VPS OVH

> Toute mauvaise manipulation entrainera une perte définitive des données. Il est très fortement recommandé de faire une sauvegarde avant de lancer cette opération !
{.is-danger}

OVH propose 5 offres VPS comprenant des disques de tailles différentes :
- STARTER : 20Go (3,50 € HT / mois)
- VALUE : 40Go (5,80 € HT / mois)
- ESSENTIEL : 80Go (12,50 € HT / mois)
- COMFORT : 160 Go (23,50 € HT / mois)
- ELITE : 320 Go (49,50 € / mois)

Si votre disque se remplit, vous avez la possibilité de passer sur un VPS de gamme supérieure en quelques clics.

Cependant, l'espace disque supplémentaire ne sera pas automatiquement alloué à votre disque actuel.
Cela nécessite une manipulation supplémentaire via la console SSH.

Commencez par stopper les services :

Si vous utilisez OPTIMUS V4
`sudo systemctl stop apache2`
`sudo systemctl stop mysql`
`sudo systemctl stop postfix`
`sudo systemctl stop dovecot`

Si vous utilisez OPTIMUS V5
`sudo docker stop optimus-drive optimus-mail`

Démontez ensuite la partition :
`sudo umount /srv`

Lancez ensuite l'utilitaire fdisk :
`sudo fdisk /dev/sda`

L'idée est maintenant de supprimer la partition puis la recréer avec une taille plus grande.
A noter qu'on supprime la partition mais cela ne signifie pas qu'on supprime les données qu'elle contient. Si tout se passe bien, on les retrouvera une fois l'opération terminée.
- tapez p pour afficher les partitions
- repérez le numéro de celle à supprimer (habituellement 2) et notez bien l'adresse du secteur de début (start)
- tapez d pour supprimer la partition (choisissez bien la bonne ! habituellement 2)
- tapez n pour créer une partition de type primaire et renseignez exactement le même numéro (habituellement 2) et le même secteur de début "start" (habituellement, il est déjà prérenseigné par défaut). fdisk proposera par défaut d'utiliser tout l'espace disponible (secteur end maximal). S'il propose de supprimer la signature crypto_LUKS répondez impérativement NON.
- tapez w pour sauvegarder les changements

A ce stade on peut redémarrer le serveur :
`sudo reboot`

Une fois le serveur redémarré, il suffit de lancer la dernière commande pour redimensionner le système de fichier : 
`sudo resize2fs /dev/mapper/cryptsda2`

Pour finir, on peut éventuellement vérifier la nouvelle structure des partitions avec la commande :
`df -h`


