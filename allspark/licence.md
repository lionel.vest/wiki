---
title: Licence
description: Licence
published: true
date: 2024-11-06T06:06:09.671Z
tags: 
editor: markdown
dateCreated: 2021-06-03T16:36:46.188Z
---

# Licence AGPL V3

Les logiciels fournis par notre association CYBERTRON, à savoir les scripts d'installation, l'api de communication et le client OPTIMUS sont publiés sous licence [libre](/fr/logiciel-libre) AGPL V3

Avec cette licence, vous êtes libre :
- d'exécuter et utiliser le logiciel gratuitement pour n'importe quel usage
- d'étudier son fonctionnement et l'adapter à vos besoins
- d'en redistribuer gratuitement des copies

Votre seule obligation est de :
- publier les modifications que vous apportez au code, ceci garantissant qu'elles profitent à tous

Comme toute licence libre, vous utilisez  les logiciels sous votre seule responsabilité, l'association CYBERTRON ou ses développeurs bénévoles ne pouvant pas être tenus pour responsables d'un éventuel dommage.

La version complète de la licence est disponible [ici](https://www.gnu.org/licenses/agpl-3.0.fr.html)