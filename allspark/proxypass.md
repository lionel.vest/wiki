---
title: Site internet hébergé ailleurs
description: 
published: true
date: 2024-11-06T06:47:10.495Z
tags: 
editor: markdown
dateCreated: 2023-02-05T19:25:53.769Z
---

# Hergement de votre site web sur un serveur externe

Les serveurs OPTIMUS offrent une espace pour y héberger votre propre site internet

Si vous préférez conserver un site internet hébergé à l'extérieur, il est recommandé d'installer le service OPTIMUS-WEBSITE. Il suffit ensuite d'aller dans Administration / Site web et de renseigner l'adresse ip de votre serveur d'hébergement. Optimus se chargera de bien paramétrer votre nom de domaine pour ne pas affecter la bonne réputation de vos emails