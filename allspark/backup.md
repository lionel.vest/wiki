---
title: Sauvegardes ses données
description: Sauvegarde
published: true
date: 2024-11-06T06:28:15.567Z
tags: 
editor: markdown
dateCreated: 2021-06-03T14:46:49.711Z
---

# Sauvegarder ses données

Rappelez-vous que votre serveur contient tous vos courriels, fichiers, factures, comptabilité, bases de données clients ...

> Assurer une sauvegarde de votre serveur n'est pas une option !
{.is-warning}

> Une sauvegarde fiable implique obligatoirement une réplique **complète** des données, sur une machine **différente**, située dans un **lieu différent**.
{.is-success}

Installer un disque miroir (RAID) prémunit d'une panne de disque mais pas d'une corruption du système, ni d'un vol, ni d'un incendie, ni d'un dégât des eaux, ni d'une erreur humaine de manipulation des fichiers.

Une sauvegarde performante peut être configurée directement chez votre hébergeur. Si vous hébergez votre serveur chez OVH, il est recommandé d'acriver la fonction backup. Une sauvegarde sera alors effectuée automatiquement une fois par jour. Vous pourrez accéder aux 7 dernières sauvegardes.
Attention : l'option "snapshot" chez OVH n'est pas à proprement parler une sauvegarde :  l'image est stockée sur le même serveur et elle ne se fait pas automatiquement.

Pour les utilisateurs avancés, notez qu'OPTIMUS intègre un système de réplication des données qui permet de réaliser des sauvegardes chaque nuit. Pensez à bien le configurer. Il s'agit de sauvegardes incrémentielles (rdiff-backup) qui se lancent chaque jour à 2h du matin. Incrémentielle signifie qu'on peut restaurer un fichier dans ses versions antérieures. Par défaut on peut ainsi remonter jusqu'à 7 jours mais il est possible de configurer une durée plus longue.

Notez qu'un serveur situé à votre cabinet peut être répliqué sur un serveur hébergé, ou inversement.

Un serveur hébergé peut être répliqué sur un autre serveur hébergé mais dans ce cas il faut veiller à ce que ce dernier soit situé dans un data center situé dans une autre ville que le premier.

Si vous disposez d'une bonne connexion internet, il est aussi envisageable de mettre le serveur de sauvegarde à votre domicile, l'encombrement d'une telle machine pouvant être réduite (15cm x 15cm). Il faut toutefois prendre en compte le risque de vol ou de détérioration, ainsi que le risque que cela représente pour la confidentialité de vos données. Partager un serveur professionnel sur un réseau local utilisé par vos enfants n'est pas nécessairement la meilleure option.