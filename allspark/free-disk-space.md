---
title: Libérer de l'espace sur le disque système
description: 
published: true
date: 2024-11-20T19:31:20.386Z
tags: 
editor: markdown
dateCreated: 2022-06-17T10:09:02.707Z
---

# Libérer de l'espace sur le disque système

Le disque système est limité à 12Gb, ce qui est suffisant pour faire fonctionner les services.

Le disque peut cependant se remplir à cause des logs. Voici deux commandes pour purger les archives :

`sudo docker image prune`

`sudo journalctl --vacuum-size=100M`

Il peut aussi être utile de vider le cache du gestionnaire de paquets :

`sudo apt clean`