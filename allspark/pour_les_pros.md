---
title: Pour les pros
description: Pour les pros
published: true
date: 2024-12-13T12:09:01.665Z
tags: 
editor: markdown
dateCreated: 2021-06-03T16:22:10.545Z
---

# Type d'installation
OPTIMUS propose deux méthodes d'installation. 

Vous pouvez :

- soit utiliser le site https://installer.optimus-avocats.fr si votre serveur et votre nom de domaine sont hébergés chez OVH
- soit procéder à une installation manuelle via des scripts SSH

Pour l'installation manuelle, il y a 3 options :
- soit installer manuellement chaque composant en les sélectionnant dans la liste. Les scripts vous poseront alors un certain nombre de questions préalables (mots de passes, clés, confirmations)
- soit installer tous les modules en mode automatique. Les scripts ne poseront alors que peu de questions et génèreront automatiquement les mots de passe.
- soit provisionner une installation en modifiant directement le fichier de configuration /root/.optimus qui contient et mémorise tous les règlages


# Organisation des dossiers sur le serveur
L'arborescence du serveur est la suivante :

/srv/db-backup contient les sauvegardes quotidiennes de la base de données
/srv/cloud contient le serveur WEBDAV
/srv/databases contient les bases de données qui seront servies via MARIA DB
/srv/files contient les fichiers des utilisateurs, qui seront servis via WEBDAV
/srv/increments contient les sauvegardes incrémentielles quotidiennes du dossier /srv
/srv/mailboxes contient les boites mail des utilisateurs, qui seront servies via IMAP
/srv/optimus contient le client web optimus
/srv/vhosts contient les configurations du serveur web NGINX
/srv/www contient un espace pour héberger votre site web


# Chiffrement
OPTIMUS intègre une solution de chiffrement des données.
Une partition disque indépendante montée sur /srv est créée, puis chiffrée avec LUKS via une clé 4096 bits.
Pour d'évidentes raisons de sécurité, la clé de déchiffrement n'est pas stockée sur la machine elle même.
La clé est elle même chiffrée et stockée sur notre serveur decrypt.cybertron.fr.
La clé chiffrée n'est communiquée à votre serveur qu'après authentification sur notre serveur et confirmation de votre part.
Ainsi, si votre serveur est volé ou redémarré, personne ne pourra accéder à vos données sans votre accord préalable.
De même, votre serveur peut être verrouillé en urgence par simple redémarrage à distance.
A noter que l'association CYBERTRON ne dispose pas de la clé mais uniquement d'une version chiffrée de la clé que seul votre serveur peut déchiffrer.
Il est donc recommandé de bien sauvegarder vos clés de chiffrement en fin d'installation. Elles sont fournies à la fin de l'installation automatisée. Veillez à ne pas les stocker dans votre boite mail ou dans un service en ligne, mais plutôt par exemple sur une clé USB stockée à votre domicile ou dans un coffre.


# Serveur mail
Le serveur mail intègre les applications suivantes :

- POSTFIX pour la distribution des courriels entrants et l'envoi des courriels sortants via SMTP sécurisé (port 587 - SSL/TLS)
- DOVECOT pour la consultation des courriels via IMAP sécurisé (port 993 - SSL/TLS)
- SPAMASSASSIN pour filtrer les courriels indésirables via des règles configurables (whitelist, blacklist, scoring)
- CLAMAV pour mettre en quarantaine les courriels qui contiennent des virus
- SIEVE pour mettre en place des règles de filtrage (par exemple le classement automatique des spams dans un dossier "Indésirables"). Ce protocole permet également de mettre en place des messages d'absence intelligents et configurables
- ACL pour partager dossiers ou sous dossiers avec d'autres utilisateurs du même domaine
- OPENDKIM et OPENDMARC pour authentifier l'expéditeur auprès des destinataires et éviter d'être classé en indésirable
- ALIASES : Une boite de messagerie peut être contactée via plusieurs adresses
- REDIRECTIONS : redirection automatique des courriels destinés à une adresse vers une ou plusieurs autres adresses
- RECIPIENT_BCC : envoi d'une copie des courriels entrants destinés à une adresse sur une seconde adresse
- SENDER_BCC : envoi d'une copie des courriels envoyés depuis une adresse sur une seconde adresse

Le serveur intègre également un webmail ROUNDCUBE compatible smartphone et agrémenté de quelques plugins utiles permettant de gérer vos règles de filtrage, votre antispam, le chiffrement de vos mails ainsi que vos messages d'absence.

Attention de bien configurer le reverse DNS de votre adresse IP sans quoi certains destinataires comme GMAIL ou YAHOO refuseront les mails que vous envoyez.

Cette précaution prise, les serveurs ALLSPARK obtiennent normalement 10/10 sur [Mail Tester](mail-tester.com)


# Cloud privé
OPTIMUS intègre un serveur cloud SABREDAV.
Cette solution permet d'accéder à vos fichiers de 3 manières :

- via n'importe quel client supportant le protocole WEBDAV, tel que RAIDRIVE sous WINDOWS, Finder sous MacOS ou DAVFS sous Linux.
- via le client web intégré à OPTIMUS
- via un navigateur en vous connectant sur https://cloud.votredomaine (mais les fonctionnalités sont limitées à la seule consultation des fichiers)


# Ouverture des ports
Si vous hébergez vous même votre serveur, votre routeur (ou votre box) doit impérativement rediriger les ports suivants vers votre serveur :

25   SMTP (pour recevoir des mails)
143  IMAP (pour consulter ses mails avec connexion TLS)
465  SMTPS (pour envoyer des mails avec connexion TLS)
587  SMTPS (pour envoyer des mails avec connextion SSL)
993  IMAPS (pour consulter ses mails avec protection SSL)
3306 MYSQL (optionnel si vous voulez vous connecter à la base de donnée depuis l'extérieur)
7822 SSH (optionnel si vous voulez vous connecter à la console SSH depuis l'extérieur)
20000 WEBSOCKET (permet au serveur d'envoyer des messages au navigateur des utilisateurs connectés)


# Support technique - remontées utilisateurs

- ajout d'un second nom de domaine

Vous pouvez demander à Optimus de gérer un second nom de domaine ce qui permet de créer des adresses @secondnomdedomaine.fr. Il faut pour ça acheter (réserver) le nom de domaine, puis le configurer pour qu'il renvoie les emails vers le serveur optimus, puis aller sur Optimus dans Administrer -> Serveur Mail -> Ajouter un domaine

- alias et redirections

indiquer à optimus que nouvelutilisateur@votredomaine.fr est un alias de votre mail actuel dans Moncompte -> Courriel -> Aliases -> Ajouter un alias. Dans ce cas les mails adressés aux deux adresses arriveront toutes dans la boite initiale
