---
title: Utiliser sa clé RPVA sur un poste Linux
description: Installer sur Linux
published: true
date: 2025-01-03T15:34:36.590Z
tags: 
editor: markdown
dateCreated: 2021-06-13T22:06:33.060Z
---

# Installer sa clé RPVA sur Linux

Les clés (aussi appelées "token") RPVA concernées par ce tutoriel et qui requièrent l'installation de pilotes spécifiques sont les modèles suivants : 
- modèle IDGO 800
- modèle IDPrime 940 (type ID Prime MD)
- modèle eToken5100

## FAIRE RECONNAITRE LA CLEF PAR VOTRE ORDINATEUR


# Tabs {.tabset}

## INSTALLATION EN MODE GRAPHIQUE (UBUNTU)

### Version 10.8

Cliquez et [téléchargez le lien](https://www.digicert.com/StaticFiles/Linux_SAC_10_8_R1_GA.zip) : 
`https://www.digicert.com/StaticFiles/Linux_SAC_10_8_R1_GA.zip`

Décompressez l'archive qui se trouve dans votre dossier de téléchargement.

Aller dans le sous-dossier : `SAC_10_8_28_GA_Build/SAC 10.8.28 GA Build/Installation/Standard/Ubuntu-2004`

Cliquez avec le bouton droit sur le fichier `safenetauthenticationclient_10.8.28_amd64.deb` 

Cliquer sur 'Ouvrir avec Installation de l'application' (ou tout autre nom apparaissant en première ligne'

Une fenêtre s'ouvre avec le nom 'safentauthenticationclient - SAS PKCS#11 middleware'

Cliquer sur Installer.

##### Si aucune erreur n'est rapportée, votre clef est installée 


#### Si ce n'est pas le cas

c'est qu'il vous manque des dépendances, car le driver fourni par le CNB a besoin de ressources logicielles externes anciennes et obsolètes qu'il faudra installer juste pour ce driver.

Vous pouvez installer les paquets manquants en remplacant  "paquet manquant" par le nom de la dépendance manquante signalée lors de l'installation :

` wget http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.0g-2ubuntu4_amd64.deb`
` sudo dpkg -i libssl1.1_1.1.0g-2ubuntu4_amd64.deb`

Relancer l'installation de SAC prévue au dessus.

La console renvoie une erreur.

Lancer la commande :

`sudo apt --fix-broken install `

et le problème est réglé ! (Merci Romain Winczewski)



#### Si c'est le cas, c'est installé !

Pour finir, il faut encore installer les certificats dans le navigateur FIREFOX ou CHROME/CHROMIUM (voir ci-après).


### Version 10.7.77

Aller sur le site de support où trouver le driver : https://www.ensured.com/support/Ensured_Downloads/Safenet_Authentication_Client_software

Cliquez sur le fichier finissant par 'amd64.deb'

![capture_d’écran_de_2021-06-18_10-35-22.png](/capture_d’écran_de_2021-06-18_10-35-22.png)

Aller dans votre explorateur de fichier, dans le dossier Téléchargements

![capture_d’écran_de_2021-06-18_10-38-02.v01.png](/capture_d’écran_de_2021-06-18_10-38-02.v01.png)

Cliquer avec le bouton droit sur le fichier.

Cliquer sur 'Ouvrir avec Installation de l'application' (ou tout autre nom apparaissant en première ligne'

Une fenêtre s'ouvre avec le nom 'safentauthenticationclient - SAS PKCS#11 middleware'

Cliquer sur Installer.

Vous devriez voir cela :

![capture_d’écran_de_2021-06-18_10-42-47.png](/capture_d’écran_de_2021-06-18_10-42-47.png)

#### Si c'est le cas, c'est installé !

#### Si ce n'est pas le cas...

C'est qu'il vous manque des dépendances, car le driver fourni par le CNB a besoin de ressources logicielles externes anciennes et obsolètes qu'il faudra installer juste pour ce driver.

Vous pouvez installer les paquets manquants en remplacant  "paquet manquant" par le nom de la dépendance manquante signalée lors de l'installation :

` wget http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.0g-2ubuntu4_amd64.deb`
` sudo dpkg -i libssl1.1_1.1.0g-2ubuntu4_amd64.deb`

Relancer l'installation de SAC prévue au dessus.

La console renvoie une erreur.

Lancer la commande :

`sudo apt --fix-broken install `

et le problème est réglé ! (Merci Romain Winczewski)



## INSTALLATION EN LIGNE DE COMMANDE (UBUNTU)

Il faut d'abord installer quelques dépendances si elles ne le sont pas déjà :

`sudo apt install pcscd pcsc-tools`

Tapez ensuite :

`systemctl status pcscd`

Dans la réponse : vérifiez que le service est bien actif :

`pcscd.service - PC/SC Smart Card Daemon`
`Loaded: loaded (/usr/lib/systemd/system/pcscd.service; indirect; vendor preset: disabled)`
`Active: active (running) since Mon 2019-05-27 22:22:27 -03; 1h 23min ago`
`Docs: man:pcscd(8)`
`Main PID: 25347 (pcscd)`
`Tasks: 8 (limit: 4915)`
`CGroup: /system.slice/pcscd.service`
`└─25347 /usr/sbin/pcscd –foreground`
           
Insérer la clé RPVA puis tapez :

`lsusb`

vérifiez que le résultat comprend bien la ligne suivante "Gemalto GemPC Key SmartCard Reader" comme dans l'exemple ci-dessous :

`Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub`
`Bus 002 Device 005: ID 08e6:3438 Gemalto (was Gemplus) GemPC Key SmartCard Reader` 
`Bus 002 Device 004: ID 0e0f:0008 VMware, Inc. VMware Virtual USB Mouse`
`Bus 002 Device 003: ID 0e0f:0002 VMware, Inc. Virtual USB Hub`
`Bus 002 Device 002: ID 0e0f:0003 VMware, Inc. Virtual Mouse `
`Bus 002 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub`

Tapez ensuite :

`pcsc_scan`

Vérifiez que le résultat comprend bien la mention "Gemalto USB Shell Token V2" comme dans l'exemple ci-dessous :

`Using reader plug'n play mechanism`
`Scanning present readers...`
`0: Gemalto USB Shell Token V2 (B21C5FA8) 00 00`
`Thu Jan 30 17:50:27 2020`
 `Reader 0: Gemalto USB Shell Token V2 (B21C5FA8) 00 00`
  `Event number: 0`
  `Card state: Card inserted, Shared Mode,`
  `ATR: 3B 7F 96 00 00 80 31 80 65 B0 85 03 00 EF 12 0F FE 82 90 00'`

Il faut ensuite télécharger le pilote que l'on trouve à l'adresse suivante : 


* version 10.7.77 :
`https://www.ensured.com/support/Ensured_Downloads/Safenet_Authentication_Client_software`
Choisissez la version correspondant à votre architecture Linux :
	- 	'.deb' pour les distribuntions Debian
	-   '.rpm' pour les distributions RedHat
	-   'I386' pour les architectures 32 bits
	-   'AMD64' pour les architectures 64 bits

Par exemple pour télécharger la version 10.7.77 du driver, destiné aux architectures debian x64 :

`wget https://www.ensured.com/files/safenetauthenticationclient_10.7.77_amd64.deb`

Il faut ensuite installer le paquet téléchargé (à modifier selon la version) :

`dpkg -i safenetauthenticationclient_10.7.77_amd64.deb`



* version 10.8.28 :
`https://knowledge.digicert.com/generalinformation/INFO1982.html`
Téléchargez la version linux :
`https://www.digicert.com/StaticFiles/SAC_10_7_Linux_GA.zip`
Dézipez le fichier, les fichiers d'installation se trouvent dans le sous-repertoire correspondant à votre architecture Linux :
`SAC_10_8_28_GA_Build/SAC 10.8.28 GA Build/Installation/`

Par exemple pour télécharger la version 10.8.28 du driver, destiné aux architectures Ubuntu 20.04 et suviantes :

`dpkg -i ~/Téléchargements/SAC_10_8_28_GA_Build/SAC 10.8.28 GA Build/Installation/Standard/Ubuntu-2004/safenetauthenticationclient_10.8.28_amd64.deb`

Pour finir, il faut encore installer les certificats dans le navigateur FIREFOX (voir ci-après).

#### S'il manque des dépendances qui bloquent l'installation


Vous pouvez installer les paquets manquants en remplacant  "paquet manquant" par le nom de la dépendance manquante signalée lors de l'installation :

Vous pouvez installer les paquets manquants en remplacant  "paquet manquant" par le nom de la dépendance manquante signalée lors de l'installation :

` wget http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.0g-2ubuntu4_amd64.deb`
` sudo dpkg -i libssl1.1_1.1.0g-2ubuntu4_amd64.deb`

Relancer l'installation de SAC prévue au dessus.

La console renvoie une erreur.

Lancer la commande :

`sudo apt --fix-broken install `

et le problème est réglé ! (Merci Romain Winczewski)


#



D'autres liens pour creuser :

- https://descartes-avocats.com/tag/rpva/
- https://adnotech.adwin.fr/post/RPVA-LINUX
- https://github.com/OpenSC/OpenSC/wiki/Installing-OpenSC-PKCS%2311-Module-in-Firefox,-Step-by-Step
- https://doc.ubuntu-fr.org/avocats_sur_ubuntu
- https://en.wikipedia.org/wiki/PKCS_11
- http://docs.oasis-open.org/pkcs11/pkcs11-base/v2.40/os/pkcs11-base-v2.40-os.html

## UN CLIENT DE GESTION GRAPHIQUE

Un GUI permettant une gestion des clefs par l'interface graphique existe également.

Il est téléchargeable ici : https://keystore-explorer.org/downloads.html

Une fois installé, il permet de vérifier l'installation et d'accéder aux informations contenues sur la clef, extraire des certificats, générer des paires de clefs...

![capture_d’écran_de_2021-06-16_11-34-32.png](/capture_d’écran_de_2021-06-16_11-34-32.png)

![capture_d’écran_de_2021-06-16_11-37-21.v01.png](/capture_d’écran_de_2021-06-16_11-37-21.v01.png)

# UTILISER LA CLEF AVEC LES LOGICIELS

# Tabs {.tabset}

## FIREFOX sur UBUNTU 22.04 et suivantes

> 28-02_2024 : le packet SNAP de Firefox ne permet pas de connecter la clef. Il faut donc désinstaller la version SNAP et installer la version DEB de Firefox.


[Mozilla vous explique](https://support.mozilla.org/en-US/kb/install-firefox-linux?) comment installer la version .deb qui permet d'éviter que la version Snap ne vous empêche d'utiliser votre clef RPVA]


Pour obtenir la version française il faut remplacer, au sein du tutorial, le nom `firefox` par `firefox-l10n-fr`



{.is-warning}


Puis lancer Firefox et suivre le processus standard :

- Aller dans Paramètres

![capture_d’écran_de_2021-06-16_11-45-34.png](/capture_d’écran_de_2021-06-16_11-45-34.png)

- Aller dans Vie privée et sécurité/Sécurité (côté droit, en bas de la page)/Certificats puis cliquez sur **Afficher les certificats.**

![capture_d’écran_de_2021-06-16_11-46-59.png](/capture_d’écran_de_2021-06-16_11-46-59.png)

- Dans l’onglet Autorités, cliquez sur Importer.

![capture_d’écran_de_2021-06-16_11-48-26.png](/capture_d’écran_de_2021-06-16_11-48-26.png)

- Importer tour à tour les quatre certificats en validant à chaque fois (touche Ok).

Liens de téléchargement des certificats :

- https://www.certeurope.fr/reference/certeurope_root_ca_3.cer
- https://www.certeurope.fr/reference/CertEurope_eID_Root.cer
- https://www.certeurope.fr/reference/certeurope_advanced_v4.cer
- https://www.certeurope.fr/reference/CertEurope_eID_User.cer

La clé toujours insérée, allez dans le menu Outils/Options du menu Firefox puis dans Vie privée/Sécurité (côté droit, en bas de page), puis cliquez sur **Périphériques de sécurité.** Dans l’onglet Certificats, cliquez sur Périphériques de Sécurité.

![capture_d’écran_de_2021-06-16_11-49-59.png](/capture_d’écran_de_2021-06-16_11-49-59.png)

- Cliquez sur Charger pour définir le nouveau dispositif. Entrez le nom du module: TKM

- Cliquez sur Parcourir et rechercher libeTPkcs11.so dans `usr/lib64`. Validez.

- En cas de soucis, vous pouvez essayer de trouver le pilote dans `usr/lib/pkcs11/`

N.B. : lors de votre connexion au RPVA, le mot de passe de votre utilisateur Ubuntu/Debian peut vous être demandé.

S’il demande à nouveau le mot de passe, il s’agit du code PIN à 4 chiffres de la clé.

Choisir le second certificat proposé (le nom le plus court).

![capture_d’écran_de_2021-06-16_11-51-02.v01.png](/capture_d’écran_de_2021-06-16_11-51-02.v01.png)

- Fermer les paramètres.

- Aller sur https://www.cnb.avocat.fr/

- Cliquer sur Connexion (en haut à droite) et "s'identifier avec E-DENTITAS"

![capture_d’écran_de_2021-06-16_11-53-59.png](/capture_d’écran_de_2021-06-16_11-53-59.png)

En cliquant sur l'identification, une boite de dialogue vous demandera votre code PIN.

> Vous êtes connectés !
{.is-success}


- Si vous êtes renvoyés sur la page de connexion OTP (celle où on vous demande votre identifiant CNBF), il faut vider l'historique de Firefox (Menu Hamburger - Historique - vider l'historique récent - sélectionner "tout")


## FIREFOX (133 sur UBUNTU 20.04.02)

Aller dans Paramètres

![capture_d’écran_de_2021-06-16_11-45-34.png](/capture_d’écran_de_2021-06-16_11-45-34.png)

Aller dans Vie privée et sécurité/Sécurité (côté droit, en bas de la page)/Certificats puis cliquez sur **Afficher les certificats.**

![capture_d’écran_de_2021-06-16_11-46-59.png](/capture_d’écran_de_2021-06-16_11-46-59.png)

Dans l’onglet Autorités, cliquez sur Importer.

![capture_d’écran_de_2021-06-16_11-48-26.png](/capture_d’écran_de_2021-06-16_11-48-26.png)

Importer tour à tour les quatre certificats en validant à chaque fois (touche Ok).

Liens de téléchargement des certificats :

- https://www.certeurope.fr/reference/certeurope_root_ca_3.cer
- https://www.certeurope.fr/reference/CertEurope_eID_Root.cer
- https://www.certeurope.fr/reference/certeurope_advanced_v4.cer
- https://www.certeurope.fr/reference/CertEurope_eID_User.cer

La clé toujours insérée, allez dans le menu Outils/Options du menu Firefox puis dans Vie privée/Sécurité (côté droit, en bas de page), puis cliquez sur **Périphériques de sécurité.** Dans l’onglet Certificats, cliquez sur Périphériques de Sécurité.

![capture_d’écran_de_2021-06-16_11-49-59.png](/capture_d’écran_de_2021-06-16_11-49-59.png)

Cliquez sur Charger pour définir le nouveau dispositif. Entrez le nom du module: TKM

Cliquez sur Parcourir et rechercher libeTPkcs11.so dans `usr/lib64`. Validez.

- En cas de soucis, vous pouvez essayer de trouver le pilote dans `usr/lib/pkcs11/`

N.B. : lors de votre connexion au RPVA, le mot de passe de votre utilisateur Ubuntu/Debian peut vous être demandé.

S’il demande à nouveau le mot de passe, il s’agit du code PIN à 4 chiffres de la clé.

Choisir le second certificat proposé (le nom le plus court).

![capture_d’écran_de_2021-06-16_11-51-02.v01.png](/capture_d’écran_de_2021-06-16_11-51-02.v01.png)


Fermer les paramètres.

Aller sur https://www.cnb.avocat.fr/

Cliquer sur Connexion (en haut à droite) et "s'identifier avec E-DENTITAS"

![capture_d’écran_de_2021-06-16_11-53-59.png](/capture_d’écran_de_2021-06-16_11-53-59.png)

En cliquant sur l'identification, une boite de dialogue vous demandera votre code PIN.

Vous êtes connectés !

- Si vous êtes renvoyés sur la page de connexion OTP (celle où on vous demande votre identifiant CNBF), il faut vider l'historique de Firefox (Menu Hamburger - Historique - vider l'historique récent - sélectionner "tout")

## CHROME / CHROMIUM (FEDORA 38) - clé RPVA 2023 (petit format)

> N.B. : l'utilisation de la nouvelle clé fournie en 2023 par le CNB ralentit à l'extrême l'utilisation de Firefox. Nous ne pouvons que conseiller Chromium dans l'attente d'une meilleure solution.
{.is-warning}


Fedora 38 comprend un gestionnaire de modules pkcs11 qui doit être utilisé de préférence pour gérer la clé RPVA (l'utilitaire modutil vient créer une entrée concurrente, ce qui n'est pas conseillé - cet utilitaire doit être réservé aux anciennes versions de Fedora - 34 et inférieures).

Après avoir installé le pilote avec le paquet .rpm (cf. ci-dessus - adaptez la procédure en ligne de commande en utilisant la commande `dnf install` au lieu de `apt install` et/ou `dpkg -i`), il faut créer un module dédié :

`sudo nano /etc/pkcs11/modules/rpva.module`

Copier ensuite ceci dans la première ligne de ce nouveau fichier : 

`module: /lib/pkcs11/libIDPrimePKCS11.so`

Puis quitter en sauvegardant le fichier : ctrl+o.

Redémarrez le navigateur et la clé devrait fonctionner !

## CHROME (90.0 sur FEDORA 34)

Contrairement à FIREFOX, Chrome ne propose aucun menu pour ajouter un pilote de clé.
Il faut donc installer le pilote en ligne de commande.

Pour commencer, il faut fermer tous les navigateurs ouverts

Il faut ensuite utiliser l'utilitaire modutil.
Il n'est pas installé par défaut.
Il est donc peut être nécessaire d'installer le paquet nss-tools qui contient modutil

`sudo dnf install nss-tools`

Voici ensuite la commande à lancer pour installer le pilote dans chrome.

`modutil -dbdir sql:/home/$USER/.pki/nssdb/ -add "RPVA" -libfile /usr/lib/pkcs11/libIDPrimePKCS11.so`

## FIREFOX (FEDORA 41) - clé RPVA eToken 5110 (petit format)

> N.B. : l'utilisation de la nouvelle clé fournie en 2023 par le CNB ralentit à l'extrême l'utilisation de Firefox. Nous ne pouvons que conseiller Chromium dans l'attente d'une meilleure solution.
{.is-warning}


> Fedora 41 comprend un gestionnaire de modules pkcs11 qui doit être utilisé de préférence pour gérer la clé RPVA, sauf pour la petite clef eToken5110 qui n'est pas (encore) compatible.
{.is-info}


Après avoir installé le pilote avec le paquet .rpm (cf. ci-dessus - adaptez la procédure en ligne de commande en utilisant la commande `dnf install` au lieu de `apt install` et/ou `dpkg -i`).

Pour utiliser le driver il faut ajouter manuellement le driver qui se trouve à cet emplacement de votre machine locale : 
> /usr/lib64/libeToken.so.10.8.1050

Pour ce faire aller au menu
> "Paramètres / Vie Privée et Sécurité / Périphériques de sécurité"

Cliquer sur "Charger"
Indiquer le chemin /usr/lib64/libeToken.so.10.8.1050 et donnez un nom au périphérique

> Après validation il faut retourner dans la fenêtre de dialogue qui aura disparu sous les autres en maintenant ensemble  les touches  Alt et  ^2^  (sélection des fenêtres de l'application)
{.is-warning}

Ensuite il faut désactiver le périphérique PKCS11 en cliquant sur "Décharger" ou "Unload" du périphérique

> Si vous relancez Firefox en mode normal, il rechargera le périphérique et il faudra le désactiver à nouveau. Par contre si vous lancez le navigateur en mode "navigation privée" il n'y aura pas besoin de désactiver à nouveau.
{.is-warning}


## THUNDERBIRD (78.8.1 sur UBUNTU 20.04.02)

Allez dans le Menu "Edition - Préférences"

Cliquez sur "Vie Privée et Sécurité"

Au niveau "Certifcats" cliquez sur "Périphériques de sécurité"

![capture_d’écran_de_2021-06-16_11-56-49.png](/capture_d’écran_de_2021-06-16_11-56-49.png)

Si vous avez effectué le processus dans Firefox, le périphique s'y affiche. Sinon il faut reproduire la mnoeuvre en chargeant le module et en indiquant le chemin où se trouve libeTPkcs11.so

Une fois l'installation terminée, vous pouvez utiliser la clef pour signer vos mails.

## LIBREOFFICE (7.1.4.2 sur UBUNTU 20.04.02)

Aller dans 'Outil' - 'Options' - 'Libreoffice' - 'Sécurité'

![capture_d’écran_de_2021-06-16_11-59-53.png](/capture_d’écran_de_2021-06-16_11-59-53.png)

Dans 'Chemin du certificat' cliquer sur 'Certificat...'

Sélectionner 'firefox:default-release'

![capture_d’écran_de_2021-06-16_12-00-40.png](/capture_d’écran_de_2021-06-16_12-00-40.png)

#### SIGNER UN DOCUMENT ODT

Le document doit avoir été sauvegardé une fois

Sélectionner dans 'Fichier' - 'Signatures numériques' - 'Signatures numériques'

![capture_d’écran_de_2021-06-16_12-02-36.png](/capture_d’écran_de_2021-06-16_12-02-36.png)

Cliquer sur 'Signer le document'

![capture_d’écran_de_2021-06-16_12-03-13.v01.png](/capture_d’écran_de_2021-06-16_12-03-13.v01.png)

ATTENTION : une boite de dialogue s'est ouverte et peut avoir été cachée par l'interface sous la boite de dialogue précédente. Elle demande le code PIN de la clef. Entrer le code et appuyer sur ENTER. Les certifcats de la clef s'affichent et sont sélectionnables

ASTUCE : AMt-œ (la touche au dessus de tabulation) permet de faire remonter la fenêtre au premier plan. Vous pouvez aussi la faire apparaitre en cliquant sur la roue dentée qui apparait dans la barre de tache de Ubuntu (sous les icones des applicatons). Vous pouvez aussi bouger la fenêtre de gestion des certifcats avec la souris, pour faire apparaitre la boite de dialogue du code en dessous.

![capture_d’écran_de_2021-06-16_12-11-06.png](/capture_d’écran_de_2021-06-16_12-11-06.png)

![capture_d’écran_de_2021-06-16_12-11-37.v01.png](/capture_d’écran_de_2021-06-16_12-11-37.v01.png)

![capture_d’écran_de_2021-06-16_12-04-47.v01.png](/capture_d’écran_de_2021-06-16_12-04-47.v01.png)


#### SIGNER UN DOCUMENT PDF

Sélectionner 'Fichier' - 'Signatures numériques' - 'Signer un PDF existant'

Libreoffice ouvrira le PDF

En haut du document, une barre bleue apparait : 'Ce PDF est ouvert en mode lecture seule pour autoriser la signature du fichier existant'. Cliquer sur le bouton à droite 'Signer le document'.

![capture_d’écran_de_2021-06-16_12-09-14.png](/capture_d’écran_de_2021-06-16_12-09-14.png)


Dans la boite de dialogue, cliquer sur 'Signer le document'.

ATTENTION : une boite de dialogue s'est ouverte et peut avoir été cachée par l'interface sous la boite de dialogue précédente. Elle demande le code PIN de la clef. Entrer le code et appuyer sur ENTER. Les certifcats de la clef s'affichent et sont sélectionnables

ASTUCE : AMt-œ (la touche au dessus de tabulation) permet de faire remonter la fenêtre au premier plan. Vous pouvez aussi la faire apparaitre en cliquant sur la roue dentée qui apparait dans la barre de tache de Ubuntu (sous les icones des applicatons). Vous pouvez aussi bouger la fenêtre de gestion des certifcats avec la souris, pour faire apparaitre la boite de dialogue du code en dessous.

Votre PDF est signé numériquement.

![capture_d’écran_de_2021-06-16_12-12-34.png](/capture_d’écran_de_2021-06-16_12-12-34.png)