---
title: C'est quoi un logiciel libre ?
description: Logiciel libre
published: true
date: 2021-06-22T21:00:56.889Z
tags: 
editor: markdown
dateCreated: 2021-06-07T09:02:15.647Z
---

# Définition du logiciel libre

Dans le monde du logiciel, il existe deux philosophies :
- celle des entreprises commerciales, qui développent des logiciels "propriétaires", dont elles gardent le fonctionnement secret afin de pouvoir générer un profit en vendant des licences.
- celle des logiciels libres, créés par des développeurs altruistes qui offrent le fruit de leur travail au public et permettent à quiconque de s'en servir sans restriction.

Le monde du logiciel libre est essentiellement basé sur le partage des connaissances. Plus de 50 millions de développeurs du monde entier collaborent ainsi sur le site www.github.com. En publiant quotidiennement leur code et améliorations, ces équipes sont parvenues à créer un système modulaire composé de milliers de briques réutilisables. Pour créer un logiciel personnalisé, il suffit aujourd'hui de se servir dans cette librairie monumentale et d'assembler ces composants.


# Les 4 libertés attachées au logiciel libre

Un logiciel est considéré comme "libre" s'il obéit aux 4 règles suivantes :
- l'utilisateur est libre d'en faire l'usage qu'il souhaite, sans restriction.
- l'utilisateur est libre de télécharger le code, le modifier ou l'améliorer à sa guise.
- l'utilisateur est libre de distribuer le logiciel dans sa version originale.
- l'utilisateur est libre de distribuer les versions modifiées qu'il a créées.


# Les avantages du logiciel libre

Choisir d'utiliser des logiciels libres offre plusieurs avantages :
- **Souplesse** : les logiciels libres sont très flexibles et interopérables. Ils peuvent remplir tous les besoins, sont aisés à intégrer, et faciles à remplacer.
- **Pérennité** : l'utilisateur garde toujours la pleine maîtrise d'un logiciel libre puisqu'il n'est pas tributaire de son éditeur. Il peut donc continuer à utiliser et corriger un logiciel libre, et ce, même si les concepteurs originels ont abandonné le projet. C'est une garantie d'indépendance technologique qui évite les manipulations commerciales des éditeurs de logiciels propriétaires qui souvent n'hésitent pas à imposer des mises à jour payantes et inutiles en abandonnant le support des versions antérieures
- **Evolutivité** : le code étant ouvert, on peut rapidement et aisément développer de nouvelles fonctionnalités, sans attendre ni être bloqués par les choix de l'éditeur. Les technologies utilisées étant ouvertes et documentées, il est très facile de trouver des professionnels pouvant maintenir ou améliorer un code pré-existant.
- **Coût** : la plupart des logiciels libres sont entièrement gratuits.
- **Sécurité** : le code étant public, des milliers de développeurs peuvent l'étudier et signaler ou corriger les failles. Ce mode de fonctionnement collaboratif a fait de Linux et ses dérivés la référence en matière de sécurité informatique.
- **Confiance** : Aucun développeur ou informaticien ne touche de commission pour vous vendre un logiciel libre. Vous êtes ainsi assuré de la neutralité absolue du conseil qui vous est donné. Si un logiciel libre a du succès, c'est qu'il est techniquement performant, et pas le fruit d'une trompeuse campagne marketing.
- **Engagement** : entrer dans le monde du logiciel libre, c'est aussi diffuser des valeurs éthiques de partage, de transparence, de liberté et de respect de l'intérêt collectif.


# Les particularités du logiciel libre
Les logiciels libres, du fait de leur fréquente gratuité, ont généralement deux particularités :
- **Absence de support 24/7** : Il n'y a habituellement pas de "hotline" pouvant intervenir à tout moment. Le support se fait via des forums de discussions communautaires, généralement en anglais, sans garantie de réponse ni de célérité. Cet ecueil est cependant aisément contournable. En effet, il existe en France des centaines de sociétés spécialisées dans le support des solutions opensource et vous pouvez facilement souscrire un contrat de support par vos propres moyens. Les logiciels libres étant documentés et utilisant des standards ouverts, n'importe quel informaticien peut s'y former rapidement. Les forums précités sont également une source inépuisable d'information pour eux.
- **Non responsabilité** : utiliser un logiciel libre se fait sous votre seule responsabilité. Aucun développeur ni entité ne saurait être juridiquement tenu pour responsable en cas de problème. C'est le corrolaire de la gratuité. Cependant, la qualité des logiciels libres est identique, voire supérieure aux logiciels propriétaires. Les défaillances sont extrèmement rares.


# Les logiciels libres les plus célèbres

Vous utilisez certainement des logiciels libres sans le savoir.

Quelques exemples : 
- Les navigateurs FIREFOX ou CHROMIUM
- Le lecteur vidéo VLC MEDIA PLAYER
- La suite bureautique LIBRE OFFICE
- Le gestionnaire d'archives compressées 7-ZIP
- Le client mail THUNDERBIRD
- Android et IOS qui sont basés sur des noyaux libres

Des logiciels libres font également fonctionner une très grande partie de vos équipements matériels (modems, box internet, objets connectés, systèmes embarqués dans vos véhicules etc ...)

Aussi et surtout, le logiciel libre fait fonctionner 90% des serveurs web de la planète. Ce cloud invisible fait fonctionner vos emails, vos espaces de stockage en ligne, les sites internet que vous consultez, vos sites de streaming, vos réseaux sociaux ou encore vos sites d'achats en ligne.

Le logiciel libre a pris une telle ampleur depuis 20 ans que même les GAFAM les plus réfractaires contribuent désormais à cette ressource universelle.


# OpenSource, OpenData, OpenStandards (ODOSOS)

L'**OpenSource** (code source ouvert) est une pratique consistant à diffuser librement le code source d'un logiciel pour notamment permettre à d'autres développeurs de l'améliorer ou le corriger. C'est généralement un corrolaire du logiciel libre.

L'**OpenData** (données ouvertes) est une pratique consistant à diffuser librement certaines données et à autoriser leur réutilisation, dans une philosophie de transparence et de partage du savoir.

Les **OpenStandards** (standards ouverts) permettent de partager facilement et fidèlement des données entre applications en utilisant des normes publiques et documentées. Ils permettent aux utilisateurs de changer aisément de logiciel et d'éviter toute entrave à la récupération de leurs données.

> Tous les développements réalisés par notre association obéissent à ces règles.
Nos logiciels sont tous diffusés sous une licence libre et n'intègrent que des technologies ouvertes.
Nos codes sources sont téléchargeables sur https://git.cybertron.fr
Notre API de traitement des données est documentée sur https://swagger.optimus-avocats.fr
Elle obéit à la norme [REST (RFC7231)](https://datatracker.ietf.org/doc/html/rfc7231) et communique au format ouvert [JSON (RFC7159)](https://datatracker.ietf.org/doc/html/rfc7159)
Nous documentons par ailleurs autant que possible nos solutions sur ce WIKI
{.is-info}
