---
title: Migration V4-V5
description: Instructions pour migrer un serveur V4 en version V5
published: true
date: 2024-12-13T16:05:46.551Z
tags: 
editor: markdown
dateCreated: 2024-11-24T11:13:51.698Z
---

# Comment migrer un serveur V4 en V5

  
Ce tutoriel s'applique aux utilisateurs qui disposent d'un serveur OPTIMUS en version 4.71 hébérgé chez OVH.

## PREREQUIS

Pour bénéficier d'une mise à jour vers OPTIMUS V5, la solution la plus simple consiste à réserver un nouveau VPS et y installer OPTIMUS V5. Ensuite un script de synchronisation de notre conception va migrer les données de votre serveur V4 vers le nouveau serveur V5.

Une fois la migration effectuée, vous pourrez résilier l'ancien serveur et conserver uniquement le nouveau.

Les pré-requis sont les mêmes que pour la V4 :
- 1 Vcore
- 2 Go de Ram
- 20 Go d'espace disque (sachant que le système prend 10 Go et que le reste de l'espace libre permet d'héberger vos fichiers et vos courriels.)

Attention : le nouveau VPS doit comporter une espace disque au moins égal à celui qu'occupe vos fichiers et courriels présents sur votre ancien VPS. 

Si vous avez souscrit un engagement de location du serveur sur 12 ou 24 mois, vérifiez bien la date d'échéance de votre abonnement avant de lancer la migration, car si vous migrez vers le nouveau serveur, vous devrez néanmoins payer la location de l'ancien jusqu'à l'échéance de votre abonnement.


## ETAPE 1 : Assurez vous de disposer d'une sauvegarde récente

Assurez vous de disposer d'une sauvegarde complète et récente de votre serveur actuel.

Chez OVH, il existe deux type de sauvegardes :
- les "Backups automatisés" : une image est créé chaque jour à heure fixe. Les sauvegardes des 7 derniers jours sont disponibles. Si vous disposez de cette option, vérifiez bien la date de la dernière sauvegarde pour vous assurer qu'elle soit récente.
- les "Snapshots" : cette option permet de créer à tout moment une image (et une seule) de votre serveur en cliquant sur le bouton. Si vous disposez de cette option, pensez à créer un "snapshot" avant de lancer la migration. Téléchargez éventuellement le snapshot par mesure de sécurité.

Voir la vidéo explicative sur le site d'OVH : https://help.ovhcloud.com/csm/fr-vps-using-snapshots?id=kb_article_view&sysparm_article=KB0047765


## ETAPE 2 : Installez OPTIMUS V5 sur votre nouveau VPS

Une fois votre nouveau VPS commandé et livré par OVH, il va falloir y installer Optimus V5.

L'installation a été grandement simplifiée par rapport à la V4.

En effet, il suffit désormais de vous rendre sur notre site d'installation automatisée  (https://installer.optimus-avocats.fr) et de suivre les étapes.

A l'étape 4, il faudra sélectionner le nom de domaine avec lequel configurer votre serveur. Choisissez le même que celui utilisé pour la V4. Notre installation automatisée configurera automatiquement la zone DNS de votre domaine de sorte qu'il n'est plus nécessaire de faire ces réglages manuellement.

> OPTIMUS V5 va prendre le contrôle de votre nom de domaine et réinitialiser les réglages. Si le domaine redirige vers d'autres services extérieurs à OPTIMUS, comme par exemple un site web, ou d'autres sous-domaines personnalisés, les réglages seront supprimés. Dans ce cas, veillez à bien sauvegarder votre ancienne zone DNS et à rétablir manuellement les enregistrements à l'issue du processus de migration.
{.is-info}

A l'étape 5, il vous sera demandé de choisir sur quel serveur lancer l'installation d'OPTIMUS V5.

> Choisissez bien le nouveau serveur que vous venez de commander ! Si vous choisissez par erreur l'ancien serveur, celui-ci sera entièrement réinitialisé et vidé ! Pour éviter toute confusion, l'étape 5 indique la date d'achat du serveur sélectionné.
{.is-warning}

Une fois l'installation terminée, vous pourrez accéder à votre nouveau serveur OPTIMUS V5 en vous connectant sur https://optimus.votrenomdedomaine


## ETAPE 3 : Installez les services OPTIMUS

Une fois l'installation d'OPTIMUS V5 terminée, il faudra activer les services nécessaires.

Pour cela, il faut vous rendre dans le menu administration situé en haut à droite de l'interface :

![admin_menu.png](/admin_menu.png)

Puis, dans l'onglet SERVICES, il faut cliquer sur "Ajouter un service"

![add_service.png](/add_service.png)

De là, il faut installer les services additionnels suivants, dans cet ordre :
- optimus-drive
- optimus-contacts
- optimus-calendar
- optimus-business
- optimus-avocats
- optimus-mail
- optimus-webmail


## ETAPE 4 : Lancement du script de synchronisation

La dernière étape consiste à lancer le script de synchronisation.

Il faut pour cela vous connecter en SSH à votre ***ancien*** serveur.

Pour rappel, le port SSH est 7822, le nom d'utilisateur est "debian", le mot de passe est celui qui vous a été fourni lorsque vous aviez installé votre serveur V4, et le "verification code" demandé est le code à 6 chiffres généré par votre application 2FAS, AUTHY ou GOOGLE AUTHENTICATOR.

> Le nom domaine ayant à ce stade été reconfiguré, vous ne pouvez plus vous connecter en SSH en tapant simplement votre nom de domaine. Il faut vous connecter en utilisant l'adresse IP du serveur, disponible sur l'interface OVH.
{.is-info}

Une fois connecté, il faut quitter le menu OPTIMUS INSTALLER en tapant "X", puis lancer la commande suivante pour télécharger le script de migration

`sudo wget -O /srv/v4v5.php https://git.cybertron.fr/optimus/optimus-installer/-/raw/main/optimus-migration/v4v5.php`

Puis tapez la commande suivante pour lancer le script de migration

`sudo php /srv/v4v5.php`

Au démarrage, le script de migration vous demandera :
- le nom de domaine du nouveau serveur V5 que vous venez d'installer
- le mode de passe d'accès SSH à ce serveur (fourni dans l'archive secret.zip téléchargée à la fin du processus d'installation d'Optimus V5, dans le fichier passwords.txt, sous la mention DEBIAN_PASSWORD)
- le "verification code" : un code à 6 chiffres généré grâce au QRCODE que vous avez scanné dans votre application AUTHENTICATOR lors de l'installation d'OPTIMUS V5.


Le script de migration va ensuite :
- connecter l'ancien serveur au nouveau
- faire une sauvegarde de votre base de donnée actuelle
- recréer sur le nouveau serveur les même utilisateurs que sur l'ancien.
- migrer les données des utilisateurs (contacts, dossiers, intervenants, factures)
- copier les courriels des utilisateurs
- copier les fichiers des utilisateurs

> En cas d'échec de copie d'une donnée, une mention apparaitra en rouge. Surveillez bien l'affichage. En fin d'installation, vous pourrez également consulter le fichier '.log' généré par le script d'installation.
{.is-info}

> En cas d'échec, le script de migration peut être relancé mais attention car il supprime, à chaque lancement, toutes les données sur le nouveau serveur (les données sur l'ancien serveur ne sont jamais affectées)
{.is-warning}


## ETAPE 5 : Vérifiez que toutes les données ont bien été migrées

Une fois la migration effectuée, vous pouvez résilier votre ***ancien*** VPS

> Avant de résilier : vérifiez bien que toutes les données de tous les utilisateurs ont bien été migrées (contacts, dossiers, factures, fichiers et courriels).
{.is-info}

> Attention de ne pas résilier le mauvais serveur, au risque sinon de perdre vos données et vos sauvegardes ! Vérifiez bien la date de création du serveur visible sur l'interface OVH.
{.is-warning}

En cas de problème, n'hésitez pas à nous contacter sur https://discord.cybertron.fr ou par mail info@cybertron.fr



