---
title: Contenu de la solution ALLSPARK
description: Contenu
published: true
date: 2024-11-06T06:05:28.791Z
tags: 
editor: markdown
dateCreated: 2021-06-07T13:17:42.873Z
---

# Système d'exploitation
OPTIMUS fonctionne avec le système d'exploitation Linux [DEBIAN](www.debian.org)

Il s'agit de la distribution la plus utilisée au monde pour un usage de serveur. Elle est réputée pour sa sécurité, son extrème stabilité et son système de mises à jour contrôlées.

Il s'agit d'un logiciel libre sous licence [GPL V2](https://www.debian.org/legal/licenses/gpl2.fr.html)

Notez que si le serveur nécessite impérativement Linux, les utilisateurs peuvent en revanche s'y connecter depuis n'importe quel système d'exploitation (Windows, MacOS, ChromeOS, Linux, Android, IOS). L'utilisation de standards ouverts permet une compatibilité maximale.


# Serveur mail
Le serveur mail intégré à notre module OPTIMUS-MAIL permet de gérer les courriels entrants et sortants.

Vos courriels sont ainsi hébergés sur un serveur qui vous appartient et dont vous avez le contrôle, ce qui assure un niveau maximal de sécurité et de confidentialité. Aucun tiers extérieur de type Gmail, Hotmail, Orange ou Free n'intervient dans le processus de réception ou d'envoi des mails.

Le serveur intègre les logiciels suivants :
- [POSTFIX](https://www.postfix.org/) sous licence [EPL V2](https://www.eclipse.org/legal/epl-2.0/)
- [DOVECOT](https://www.dovecot.org/) sous licence [MIT et LGPL](https://dovecot.org/doc/COPYING)
- [SPAMASSASSIN](https://spamassassin.apache.org/) sous licence [APACHE](https://www.apache.org/licenses/)
- [CLAMAV](https://www.clamav.net/) Antivirus sous licence [CC BY-ND 2.5](https://creativecommons.org/licenses/by-nd/2.5/)


# Webmail
OPTIMUS intègre le webmail [ROUNDCUBE](https://roundcube.net/) sous licence [GPL V3](https://github.com/roundcube/roundcubemail/blob/master/LICENSE) qui vous permet de consulter et envoyer vos mails depuis n'importe quel navigateur ou smartphone.

Nous l'avons complété avec des modules additionnels très utiles : 
- [SAUSERPREFS](https://plugins.roundcube.net/#/packages/johndoh/sauserprefs) pour configurer facilement l'antispam intégré
- [ENIGMA](https://github.com/roundcube/roundcubemail/tree/master/plugins/enigma) pour échanger des mails chiffrés (cryptés)
- [CONTEXTMENU](https://plugins.roundcube.net/#/packages/johndoh/contextmenu) qui ajoute des fonctionnalités de manipulation des messages et dossiers
- [HOTKEYS](https://plugins.roundcube.net/#/packages/random-cuber/hotkeys) qui permet de configurer des raccourcis clavier.
- [MANAGESIEVE](https://github.com/roundcube/roundcubemail/wiki/Plugin-managesieve) qui permet de créer des filtres ainsi qu'un message d'absence automatique


# Serveur de fichiers, contacts et agendas
OPTIMUS intègre un serveur [SABREDAV](https://sabre.io/dav/) sous licence [BSD](https://sabre.io/license/) qui permet de distribuer :
- vos fichiers, via le protocole WEBDAV
- vos agendas, via le protocole CALDAV
- vos contacts, via le protocole CARDDAV

Voir [cette page](/fr/allspark/webdav_connect) pour connecter ce cloud privé et le voir apparaître comme un disque sur votre système d'exploitation.


# Serveur de bases de données
OPTIMUS intègre un serveur de base de données [MARIADB](https://mariadb.org/) sous licence [GPL V2](https://mariadb.com/kb/en/mariadb-license/)


# Serveur web
OPTIMUS intègre le serveur web [NGINX](https://nginx.org/) sous licence [BSD](https://nginx.org/LICENSE)


# Sauvegardes automatisées
OPTIMUS intègre un système de sauvegardes incrémentielles externalisées basé sur le logiciel [RDIFF-BACKUP](https://rdiff-backup.net/) sous license [GPL V2](https://github.com/rdiff-backup/rdiff-backup/blob/master/COPYING)


# Sécurité
Pour améliorer la sécurité du système, OPTIMUS intègre :
- le pare-feu [UFW](https://launchpad.net/ufw) sous licence [GPL V3](https://git.launchpad.net/ufw/tree/COPYING)
- la protection [FAIL2BAN](https://www.fail2ban.org/) sous licence [GPL V2](https://github.com/fail2ban/fail2ban/blob/master/COPYING) qui bloque les attaques externes
- [AUTHENTICATOR](https://github.com/google/google-authenticator) sous license [APACHE V2](https://github.com/google/google-authenticator/blob/master/LICENSE) qui ajoute un deuxième facteur d'authentification, en l'espèce un code à 6 chiffres qui change toutes les 30 secondes
- [KNOCKD](http://www.portknocking.org/) sous license [GPL V3](https://git.launchpad.net/ufw/tree/COPYING) qui ajoute un troisième facteur d'authentification, à savoir un code d'entré caché qui permet d'activer le serveur SSH en appelant une séquence de ports TCP ou UDP

