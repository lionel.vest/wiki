---
title: Documentation de l'api
description: 
published: true
date: 2024-11-06T07:02:48.207Z
tags: 
editor: markdown
dateCreated: 2021-06-11T13:48:36.937Z
---

# API de communication

OPTIMUS est construit autour d'une API (Application Programming Interface) qui est documentée ici : https://optimus.demoptimus5.ovh/optimus-devtools/api-doc#presentation

Elle obéit à la norme [REST (RFC7231)](https://datatracker.ietf.org/doc/html/rfc7231) et communique au format ouvert [JSON (RFC7159)](https://datatracker.ietf.org/doc/html/rfc7159)

Pour vous aider à l'utiliser, nous avons mis en place une documentation interactive [Swagger](https://optimus.demoptimus5.ovh/optimus-devtools/swagger-menu).

N'hésitez pas à venir échanger avec les développeurs du projet sur https://discord.cybertron.fr