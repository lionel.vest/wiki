---
title: Installation
description: Installation
published: true
date: 2024-11-06T06:18:08.583Z
tags: 
editor: markdown
dateCreated: 2021-06-05T07:37:27.099Z
---

# Installation automatisée OVH

Si vous êtes équipé d'un serveur VPS chez OVH, vous pouvez utiliser notre processus simplifié d'installation via notre site https://installer.optimus-avocats.fr

Il suffit de suivre les étapes à l'écran.


# Installation manuelle

Si vous n'avez pas opté pour un serveur VPS chez OVH, il faut procéder à une installation manuelle.

Cette méthode est plus complexe et nécessite de savoir manier la console SSH. Elle est conçue pour toute machine tournant sur DEBIAN 12. Elle est notamment utile si vous hébergez Optimus sur une machine local ou que votre hébergeur n'est pas OVH.

La première étape consiste à lancer les scripts d'installation de notre conception qui vont installer tous les logiciels requis et les paramétrer automatiquement.

Le plus simple pour contrôler un serveur DEBIAN est de s'y connecter avec le logiciel [PUTTY](https://www.putty.org/)
L'identifiant est "debian".
Le mot de passe est celui que vous avez renseigné lors de l'installation (NUC) ou celui qui vous a été envoyé par mail (hébergement VPS)
Une fois connecté au terminal, assurez vous que la fenêtre est en plein écran (nécessaire plus tard pour scanner le QRCODE)
Voici ensuite la commande à taper pour installer les scripts OPTIMUS

  `wget https://git.cybertron.fr/optimus/optimus-installer/-/raw/main/install.sh`
  `sudo bash install.sh -g`
  `sudo /etc/optimus/menu.sh`
  
Le menu OPTIMUS se lance alors sur votre machine et il suffit de suivre les directives qui apparaissent à l'écran.

## Installation guidée

Sauf pour certaines utilisations avancées, il est recommandé de procéder à une installation guidée en appuyant sur Z.

Il vous sera ensuite demandé : 
- le nom de domaine que vous avez réservé et qui sera connecté au serveur. Tapez ici le nom du domaine (exemple : votredomaine.com)
- le nom du premier utilisateur qui sera créé sur le système. Tapez ici son nom, étant précisé qu'il sera alors généré un compte sous nomdelutisateur@votredomaine.com

## Partitionnement du disque

La première étape de l'installation consiste à diviser le disque en deux. Une partie système et une partie pour les données. 

A l'issue de cette étape, le serveur redémarre et il faut s'y reconnecter avec putty, puis relancer l'installation automatisée en tapant Z.

L'installation se poursuivra là où elle a été interrompue par le redémarrage

## Installation des paquets

Les scripts d'installation vont ensuite installer tous les paquets requis, et préconfigurer tous les logiciels pour qu'ils fonctionnent en parfaite harmonie.


## Configuration de la Zone DNS
Cette étape primordiale consiste à créer plusieurs sous domaines et à les relier à votre serveur.
Les sous-domaines en question sont les suivants :

- api.votredomaine
- cloud.votredomaine
- mail.votredomaine
- optimus.votredomaine
- webmail.votredomaine

Lors de cette étape, il s'agit également d'ajouter dans la zone DNS les enregistrement requis pour authentifier vos mails sortants (SPF, DKIM et DMARC)

Malheureusement cette étape ne peut pas être automatisée puisqu'elle nécessite de vous connecter auprès du registrar qui gère votre domaine et d'ajouter les enregistrements requis dans la zone DNS.

Pour vous aider les scripts d'installation génèrent le fichier de zone DNS qu'il vous suffit de copier puis de coller en mode texte sur l'interface de votre registrar.

Voici un exemple de fichier de zone :
> $TTL 3600
> @	IN SOA dns113.ovh.net. tech.ovh.net. (2020110700 86400 3600 3600000 60)
>                        IN NS     ns113.ovh.net.
>                        IN NS     dns113.ovh.net.
>                        IN MX     50 mail.testoptimus.ovh.
>                        IN A      51.91.158.47
>                        IN TXT    "v=spf1 mx ~all"
> _dmarc                 IN TXT    "v=DMARC1;p=quarantine;sp=quarantine;pct=100;adkim=r;aspf=r;fo=1;ri=86400;
rua=mailto:prime@testoptimus.ovh;ruf=mailto:prime@testoptimus.ovh;rf=afrf"
> api                    IN A      51.91.158.47
> cloud                  IN A      51.91.158.47
> mail                   IN A      51.91.158.47
> mail._domainkey        IN TXT    ( "v=DKIM1; h=sha256; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsWG52L8knUWlwQXG
kd0kwtcyFMp64dQJfLyZlpSuSmR1bOVRfADwyUrbsxUDd9FIb/wbrxcJUMNtc86qP
xRiuGv5IQ3FfPUnzWSRzXwg7KJ/AP9K678/n1gD9j1cy6AeyRXZ326QGn4fy5i0WJ
5EpX5MfpwgvTtHd+yMxwab19ppQtCQGA" "Sd7W7ssOgREqN7bqi+xKNkuEqGXPZDQGXnsR7ekA+R2CiS7Rbr3KcXO+pu9pgMOi2
VoVd3Co5j2CREY3TC7eb5k9HySMpUjErMGbaxiMKpR6PldLJ+uBhF9fuXG3WE3LXWo
nAjTkbqOi0TLta5k62hErHE58Z1GZ6v0QIDAQAB" )
> webmail                IN A      51.91.158.47
{.is-info}


Ainsi par exemple, si vous avez réservé votre domaine chez OVH il faut vous rendre dans WEB CLOUD -> Noms de domaine -> votredomaine.com -> onglet Zone DNS -> Modifier en mode textuel. Il suffit alors de copier le modèle proposé par l'installation ALLSPARK et la reporter dans la fenêtre, en prenant bien soin de finir par un retour à la ligne.

![zone-dns-1.png](/zone-dns-1.png)
![zone-dns-2.png](/zone-dns-2.png)


## Configuration du reverse dns

Il faut ensuite configurer le reverse dns sur votre servuer.

Si vous avez réservé un serveur VPS chez OVH il faut vous rendre dans BARE METAL CLOUD -> Serveurs privés virtuels -> votre serveur -> IPV4 -> Gérer mes IP

![rdns-1.png](/rdns-1.png)

Sur cette page il faut ensuite modifier le "reverse" de l'adresse IPV4 pour y mettre : votredomaine.com. Attention de bien terminer par un point

![rdns-2.png](/rdns-2.png)

## Certificats SSL
Une fois la zone DNS et le reverse DNS configurés, les scripts ALLSPARK génèrent et installent des certificats SSL gratuits via LETSENCRYPT.

Ces certificats se renouvellent automatiquement tous les 90 jours.


## Authentification deux facteurs
L'installation suite ensuite son cours jusqu'à ce que s'affiche un QR CODE comme suit : 

![qrcode.png](/qrcode.png)

IL faut ensuite intégrer ce QRCODE avec l'application 2FAS sur votre smartphone

> La version Android est téléchargeable [ici](https://play.google.com/store/apps/details?id=com.twofasapp)
> 
> La version pour IOS (Iphone) est téléchargeable [ici](https://itunes.apple.com/us/app/2fas-auth/id1217793794?mt=8)
{.is-info}

Pour ce faire Il suffit de cliquer sur l'icone + en bas à droite de l'application.

## Finalisation de l'installation

Après avoir finalisé l'installation, il est vivement recommandé de sauvegarder la configuration et les clés de chiffrement. Les instructions pour se faire apparaîtront à l'écran.

Ne les conservez pas sur le serveur lui-même, dans vos mails ou dans le cloud. Conservez les de préférence sur une ou plusieurs clés USB déconnectées du réseau et en lieu sûr.

> Si vous perdez vos clés de chiffrement, vos données seront irrémédiablement perdues.
{.is-danger}

## Première connexion

Une fois l'installation terminée, le serveur va redémarrer une dernière fois.

Vous pourrez alors vous connecter sur https://optimus.votredomaine.com et vous connecter avec le compte que vous avez créé au lancement de l'installation.


## Se connecter au serveur en SSH après installation

En SSH, avec un client adapté (type PUTTY) ou directement en console :

- serveur : adresse IP du serveur
- port : 7822
- user : debian
- Mot de passe : fourni à la fin de l'installation.






