---
title: L'association CYBERTRON
description: L'association
published: true
date: 2021-06-21T20:22:53.718Z
tags: 
editor: markdown
dateCreated: 2021-06-07T12:53:28.687Z
---

# Présentation
CYBERTRON est une association à but non lucratif constituée en 2020.

Elle a été créée à l'initiative de 7 avocats du Grand Est.

Son objet statutaire est : 
- de promouvoir et défendre les valeurs du Logiciel Libre, de l’Open Source, de l’Open Data ainsi que l’interopérabilité et les standards ouverts
- soutenir et développer des logiciels libres et open source, notamment à destination des professionnels
- permettre à l’ensemble des acteurs des projets soutenus par l’association d’échanger et de travailler ensemble dans le respect de valeurs communes, autour d’outils numériques partagés
- de diffuser les savoirs et les pratiques acquises au sein de l’association auprès des autres acteurs de la société, du local à l’international, notamment par l’éducation populaire, des rencontres, actions de communication et de formation.
- de proposer des services de formation et de support technique à destination des utilisateurs des logiciels libres.

Elle est actuellement présidée par Antoine BON, Avocat au Barreau de Strasbourg

Consultez les [Statuts](/statuts_cybertron.pdf) et le [Règlement Intérieur](/ri_cybertron.pdf)



