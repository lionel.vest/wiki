---
title: Bienvenue
description: Bienvenue
published: true
date: 2024-11-06T04:31:31.384Z
tags: 
editor: markdown
dateCreated: 2021-06-05T07:28:49.978Z
---

![logo_cybertron.png](/logo_cybertron.png =500x)

#
Bienvenue sur notre WIKI !

CYBERTRON est un association à but non lucratif dont l'objet est de promouvoir et concevoir des logiciels libres.

Vous retrouverez dans cet espace des astuces pratiques ainsi que la documentation de toutes les applications développées par nos membres.

> Venez également échanger avec nous sur notre réseau social privé [HumHub](https://hub.cybertron.fr/) ou sur [Facebook](https://www.facebook.com/groups/1341013276068344)
 

# OPTIMUS
Le summum de l'opensource réuni dans une solution unique, performante et gratuite.
Hébergez vos fichiers, emails, agendas, contacts et base de données sur votre propre cloud privé.
Partagez ces données de manière sécurisée avec d'autres applications grâce à notre API de communication intégrée.
[Presentation de la solution](/fr/allspark/presentation)

# OPTIMUS AVOCATS
Il s'agit d'un module complémentaire de gestion de cabinet d'avocats directement connecté avec votre serveur OPTIMUS.
Gérer vos dossiers, vos clients, vos agendas, éditez vos fiches de temps, vos factures, pilotez votre cabinet et bien plus encore.
[www.optimus-avocats.fr](https://www.optimus-avocats.fr)

# LE LOGICIEL LIBRE
Pourquoi payer des logiciels quand il existe des alternatives équivalentes et gratuites ?
Abandonnez Microsoft Office et découvrez LibreOffice.
Libérez vous et remplacez Windows ou MacOS par Linux. 
Ces solutions ne sont plus réservées aux experts.
[C'est quoi un logiciel libre ?](/fr/logiciel-libre)
On vous explique ici comment les mettre en oeuvre facilement.


# SOCIETE.NINJA
Notre site exploitant l'opendata des entreprises
Toutes les infos du RCS, du registre des associations, du régistre des métiers, du registre des bénéficiaires effectifs...
Téléchargez gratuitement statuts, bilans, PV d'assemblées, conventions collectives et autres documents.
[www.societe.ninja](https://www.societe.ninja)

# JURISPRUDENCE.NINJA
Notre site exploitant l'opendata des décisions de justice.
Accédez aux 6.000.000 de décisions qui seront mises en lignes par le Ministère de la Justice entre septembre 2021 et fin 2025.
[www.jurisprudence.ninja](https://www.jurisprudence.ninja) (en cours de construction)

# CALCUL.NINJA
Notre site modulaire intégrant divers calculateurs :
- intérêts de retard
- dépens taxables
- indexation de loyers
- indexation de pension alimentaire
- heures supplémentaires

[www.calcul.ninja](https://calcul.ninja) (en cours de construction)

# RPVA
Saviez vous que votre clé RPVA peut être utilisée pour signer des documents PDF ?
Savez vous comment sauvegarder tous vos messages RPVA en 2 clics ?
Retrouvez ici nos astuces.
Le CNB interdit aux avocats utilisateurs d'Optimus-Avocats de se connecter au RPVA via ce logiciel. N'hésitez pas [à signer notre pétition](https://www.change.org/p/conseil-national-des-barreaux-un-acc%C3%A8s-individuel-%C3%A0-l-api-rpva-pour-chaque-avocat).