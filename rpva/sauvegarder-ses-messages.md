---
title: Sauvegarder ses messages RPVA
description: Sauvegarder ses messages RPVA
published: true
date: 2021-06-13T21:50:28.613Z
tags: 
editor: markdown
dateCreated: 2021-06-13T21:45:16.083Z
---

# Sauvegarder ses messages RPVA

Lorsque votre messagerie RPVA est pleine, les messages les plus anciens sont automatiquement supprimés. Compte tenu de la faible capacité des boîtes, cela peut arriver très vite, parfois même avant qu'une instance se soit terminée.

Cela pose un vrai problème : comment justifier d'une transmission si la preuve a disparu ?

Vous avez peut être remarqué qu'on pouvait télécharger ses messages pour les stocker ailleurs en cliquant sur cette petite icone :
![rpva-save-1.png](/rpva-save-1.png)

Problème : par défaut, on ne peut cocher que 10 messages.
On ne peut donc les sauvegarder que 10 par 10. C'est très laborieux.

Vous aurez peut être remarqué qu'on peut demander à la messagerie d'afficher les messages 50 par 50 via le menu déroulant en bas à droite : 

![rpva-save-2.png](/rpva-save-2.png)

On peut donc les sauvegarder 50 par 50. C'est un peu mieux.
Mais quand on a 2000 messages, ca reste laborieux.

Voici l'astuce pour afficher tous vos messages d'un coup et ainsi les sauvegarder tous en une seule fois : 
- avec le navigateur FIREFOX, CHROME, CHROMIUM ou BRAVE, tapez sur la touche F12 du clavier
- dans la fenêtre qui s'ouvre, allez sur l'onglet "Console"
- dans la console, tapez mail_nbMailGenericParPage=999999; gotoInboxWithParam(0,"dateMsg","DESC"); comme ci-dessous :
![rpva-save-2.png](/rpva-save-3.png)
- appuyez ensuite sur la touche "Entrée" de votre clavier

Voilà ! Votre message RPVA affiche désormais 999999 messages d'un coup.
Il ne vous reste plus qu'à cliquer sur "Tous" pour tout sélectionner, puis sur "Enregistrer"

![rpva-save-2.png](/rpva-save-4.png) ![rpva-save-1.png](/rpva-save-1.png)