---
title: Aidez-nous !
description: Aidez-nous !
published: true
date: 2024-11-06T04:38:50.326Z
tags: 
editor: markdown
dateCreated: 2021-06-07T13:02:21.519Z
---

# Contribuer
CYBERTRON est un espace collaboratif.

N'hésitez pas à contacter info@cybertron.fr si vous voulez participer à l'un ou l'autre de nos projets, nous soumettre des idées, ou même développer un projet au sein de notre communauté.

# Financer
L'association met à disposition différents outils dont elle assume la maintenance et la gestion, à ses frais : 
- un dépôt logiciel : https://git.cybertron.fr
- un wiki : https://wiki.cybertron.fr
- un espace de discussion : https://discord.cybertron.fr
- un site de démonstration de la solution OPTIMUS : https://demo.optimus-avocats.fr
- le site internet https://www.societe.ninja
- le site internet https://www.jurisprudence.ninja
- le site internet https://www.calcul.ninja

Tous nos outils sont mis à disposition gratuitement.
Nos développeurs sont bénévoles.
L'association n'a aucune source de revenus autre que les dons et cotisations.

N'hésitez pas à nous soutenir :

> Faites un don par CB ou PAYPAL : 
[![Image](https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/donate?hosted_button_id=34ALNCBUESSLS&source=url)
{.is-info}

> Faites un don en BITCOIN : 
![Image](https://www.optimus-avocats.fr/assets/img/optimus-btc-public.png =x200)
1FaNy6RnqzN31enRcKMdJ3MoiCh86NEJTp 
{.is-info}

Les sommes collectées serviront à financer les projets suivants :
- l'hébergement annuel de nos serveurs
- le renouvellement des noms de domaines
- le développement de l'ambitieux projet jurisprudence.ninja qui vise à exploiter les 6.000.000 de décisions de justice que l'Etat français est en train de mettre à disposition en Opendata.
- un audit RGPD pour le projet OPTIMUS
- au audit sécurité par un prestataire extérieur pour le projet OPTIMUS
- le développement d'OPTIMUS V5
- l'intégration du RPVA à OPTIMUS (étant précisé que le CNB exige que l'association paie une licence d'utilisation comme n'importe quel éditeur)
- la certification ISO 9001 et NF525 du logiciel OPTIMUS