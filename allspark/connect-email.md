---
title: Connecter sa boite mail
description: 
published: true
date: 2024-11-25T20:49:34.944Z
tags: 
editor: markdown
dateCreated: 2022-09-30T20:00:16.166Z
---

De manière générale, nous recommandons de n'utiliser que le webmail intégré à votre serveur Optimus qui est pré-configuré pour éviter de nombreuses erreurs de configuration.

Néanmoins, vos courriels peuvent être consultés et envoyés via n'importe quel logiciel client (OUTLOOK, THUNDERBIRD, K9 MAIL ou IPHONE) avec les paramètres suivants :

**Réception des emails**

> type de serveur : IMAP
> serveur entrant : imap.votredomaine
> port : 993
> sécurité : SSL/TLS
> méthode d'authentification : mot de passe normal.

**Envoi des emails** 

> type de serveur : SMTP
> serveur sortant : smtp.votredomaine
> port : 587
> sécurité : SSL/TLS
> méthode d'authentification : mot de passe normal

**Réputation de vos emails** 
> Il est important de bien configurer votre compte mail dans votre client.
A la moindre erreur, le système antispam de votre destinataire peut se déclencher et refuser votre messages.
Pour tester votre réputation, vous pouvez utiliser le site www.mail-tester.com.
Si vous n'obtenez pas une note de 10/10, il faut impérativement corriger votre configuration.
Attention de bien tester l'envoi au départ de chacun de vos clients (webmail, smartphone, outlook) car les résultats peuvent varier.
{.is-warning}

Voici les erreurs récurrentes qui peuvent engendrer une baisse de réputation dans les systèmes antispam :
- vous utilisez une adresse @votredomaine.com mais vous envoyez vos messages avec un autre serveur d'envoi, par exemple celui de votre fournisseur d'accès free.fr ou orange.fr. Il faut impérativement toujours envoyer vos courriels avec le serveur sortant votredomaine.com. Si vous configurez plusieurs comptes dans votre client, veillez à ce que chaque compte utilise bien le bon serveur d'envoi.
- si vous ajoutez une image dans votre signature, elle peut engendrer une perte de points si elle est mal configurée. Le site www.mail-signatures.com/signature-generator/ permet de générer des signatures mail valides.
- lorsque vous envoyez un courriel, ne changez jamais les cases "expéditeur" ou "répondre à". L'expéditeur ou l'adresse de réponse doivent impérativement être la même que celle du compte avec lequel vous envoyez les messages. N'utilisez jamais un alias pour l'envoi d'un mail. N'utilisez les alias que pour la réception.

En dépit de ces recommandations, si vous ne parvenez pas à obtenir 10/10, n'hésitez pas à nous contacter sur facebook.cybertron.fr ou discord.cybertron.fr, en nous joignant éventuellement le lien du test que vous avez préalablement réalisé avec mail-tester.com.



