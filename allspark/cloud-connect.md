---
title: Connecter son cloud privé ALLSPARK à son ordinateur
description: Connecter son cloud privé
published: true
date: 2024-11-06T06:31:45.344Z
tags: 
editor: markdown
dateCreated: 2021-06-11T12:07:01.772Z
---

# Le cloud privé intégré

OPTIMUS intègre un serveur SABREDAV qui permet de stocker et héberger vos fichiers.

C'est un service similaire à ce que propose Google Drive, Microsoft OneDrive ou Dropbox.

La seule différence - et elle est de taille - c'est que vos données ne sont pas hébergées sur les serveurs d'une multinationale américaine qui se permet d'exploiter les données que vous y mettez, mais sur un serveur privé, sécurisé et chiffré, dont vous êtes le seul propriétaire.

Le partage de fichier se fait via le protocole WEBDAV. C'est un standard ouvert qui est compatible avec absolument toutes les plateformes. Ainsi, une fois votre serveur installé, il existe diverses manières d'accéder à votre espace cloud. Nous les avons décrites dans les onglets ci-dessous.


# Tabs {.tabset}
## Windows

Windows est directement compatible avec le protocole webdav mais la façon dont le système le gère devient rapidement insupportable car il vous demande le mot de passe de connexion à chaque ouverture de fichier.

Nous recommandons en conséquence d'utiliser un logiciel tiers gratuit dénommé [Raidrive].

Vous pouvez le télécharger via le lien suivant :
(https://app.raidrive.com/download/raidrive/release/RaiDrive_2023.9.250_x64.exe) 

Une fois installé, pour connecter le cloud, il suffit d'aller dans AJOUTER -> NAS -> WEBDAV pour atteindre la page suivante : 

![raidrive-1.png](/raidrive-1.png)

Voici les champs à remplir :

- Adresse : https://cloud.votredomaine.com (exemple : https://cloud.demoptimus.fr)
- Nom utilisateur : l'adresse email de votre compte allspark (exemple : demo@demoptimus.fr)
- Mot de passe : le mot de passe de votre compte allspark (exemple : 4f0JDn9JWTqlFVHrUihyybNVWJpF98hY)

Le cloud apparaitra ensuite sur votre ordinateur comme un disque réseau où vous pourrez envoyer, télécharger, couper, coller, ouvrir comme s'il s'agissait d'une clé USB connectée à votre ordinateur.

## MacOS

MacOS X gère nativement le protocole WEBDAV via le Finder.

- Choisissez : Aller > Se connecter au serveur
- Saisissez l’adresse du serveur dans le champ "Adresse du serveur" (exemple : https://cloud.demoptimus.fr)
- puis cliquez sur "Se connecter".

L'identifiant et le mot de passe vous seront ensuite demandés lors de la première connexion.
Il s'agit de vos identifiants optimus (email et mot de passe)

## Linux

Sur GNOME, l'explorateur NAUTILUS est directement compatible avec la norme WEBDAV.
Il suffit d'utiliser la fonction "Connexion à un Serveur" au bas de la fenêtre.
Seule subtilité, l'adresse doit commencer par davs:// au lieu de https://

Par exemple : 

- pour la démo d'optimus : `davs://cloud.demoptimus.fr`
- pour l'accès optimus d'un serveur à l'adresse "votrecabinet-avocat.fr" : `davs://cloud.votrecabinet-avocat.fr` avec le nom d'utilisateur `utilisateur@votrecabinet-avocat.fr` et son mot de passe.

![nautilus-1.png](/nautilus-1.png)

Pour les adeptes de "mount" et de la ligne de commande, Linux peut se connecter à un serveur webdav en utilisant [davfs2](https://doc.ubuntu-fr.org/davfs2)

## Android

Sur Android, le logiciel gratuit [Total Commander](https://play.google.com/store/apps/details?id=com.ghisler.android.TotalCommander&hl=fr) gère le protocole WEBDAV.

Il faut toutefois installer le plugin supplémentaire [WebDAV plugin](https://play.google.com/store/apps/details?id=com.ghisler.tcplugins.WebDAV&hl=fr)

![total-commander-1.png](/total-commander-1.png)

## Iphone et Ipad

Section à venir

## Optimus
Notre logiciel de gestion de cabinet OPTIMUS intègre un navigateur WEBDAV intégré via l'icone suivante :

![optimus-fichiers.png](/optimus-fichiers.png)

Il intègre la plupart des fonctionnalités attendues d'un explorateur de fichier (ajouter un fichier, télécharger, couper, coller etc ...)

Etant encore en développement, il reste moins performant qu'une connexion directement intégrée dans votre système d'exploitation mais ce nouveau système va l'avenir permettre de nombreuses nouveautés comme :
- l'édition en ligne des documents
- la génération de modèle d'actes
- le partage de fichier ou de dossiers à des clients ou confrères.

Notre ambition est de totalement remplacer votre explorateur de fichier habituel par cette solution 100% web.

## Navigateur

Il faut savoir qu'il est possible de naviguer dans vos fichiers en vous connectant directement avec un navigateur sur https://cloud.votredomaine.com (exemple : https://cloud.demoptimus.fr)

![fichiers-firefox.png](/fichiers-firefox.png)

Cette solution est néanmoins très limitée car elle permet tout au plus de télécharger rapidement un fichier.

Elle peut néanmoins dépanner dans l'urgence et a l'avantage de fonctionner également sur tablettes et smartphone sans besoin d'installer ou de configurer quoi que ce soit.



