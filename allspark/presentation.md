---
title: Présentation
description: Présentation
published: true
date: 2024-11-06T06:01:38.809Z
tags: 
editor: markdown
dateCreated: 2021-06-03T13:27:37.280Z
---

# Présentation de la solution

OPTIMUS est un système permettant d'héberger facilement vos données sur un serveur sécurisé, qui vous appartient, et sans recourir aux services des GAFAM (Google, OneDrive, DropBox, Microsoft etc ...)

Chaque utilisateur peut ainsi se doter d'un un cloud privé dont il est le seul gestionnaire et qui garantit la confidentialité de ses données.

C'est un système modulaire qui se base sur les principaux modules suivants :
- OPTIMUS DRIVE (pour stocker vos fichiers)
- OPTIMUS CALENDAR (pour gérer vos agendas)
- OPTIMUS CONTACTS (pour gérer vos contacts)
- OPTIMUS MAIL (pour gérer vos courriels)
- OPTIMUS BUSINESS (pour gérer vos factures et votre comptabilité)

S'y ajoutent des services additionnels, spécialement conçus pour facilier l'exercice de certaines professions :
- OPTIMUS AVOCATS (logiciel de gestion de cabinet d'avocats)


> Notre modèle décentralisé assure qu'aucune de vos données ne transite jamais par les serveurs de l'association CYBERTRON ou par des sites tiers.
{.is-info}


OPTIMUS est la réunion de plusieurs logiciels libres et open source, parmi les plus éprouvés et utilisés au monde, ce qui garantit une gratuité totale, une sécurité maximale et une transparence absolue.

La liste complète des logiciels fournis figure sur la page [contenu](/fr/allspark/contenu).

L'apport de notre association CYBERTRON est essentiellement d'avoir réussi à interconnecter ces différents logiciels entre eux et d'en avoir facilité l'installation via une procédure simplifiée. CYBERTRON a également créé une API qui permet à votre serveur de communiquer via des standards ouverts et sécurisés. C'est notamment cette API qui permet à notre module de gestion de cabinet [Optimus](https://www.optimus-avocats.fr) d'interagir avec vos données.


