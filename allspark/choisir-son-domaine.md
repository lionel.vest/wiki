---
title: Choisir son nom de domaine
description: Choisir son domaine
published: true
date: 2021-06-10T21:49:41.368Z
tags: 
editor: markdown
dateCreated: 2021-06-09T22:49:15.509Z
---

# Choisir son nom de domaine

Un nom de domaine est une adresse unique du type moncabinet.com ou dupond-avocat.fr qui sera le point de connexion entre votre serveur et le reste du monde. 

C'est ce domaine qui permettra de vous connecter à votre serveur de manière sécurisée, via une connexion chiffrée (HTTPS).

Vous pouvez réserver n'importe quel domaine qui n'est pas déjà pris.

Lorsque vous êtes propriétaire d'un domaine, vous pouvez créer des adresses mail terminant par ce domaine comme jean.dupond@dupond-avocat.fr. Vous pouvez également créer des sous-domaines comme www.dupond-avocat.fr ou cloud.dupond-avocat.fr.

Un nom de domaine se réserve auprès d'une société qu'on appelle Registrar, tels que les français [OVH](https://www.ovhcloud.com/) ou [GANDI](https://www.gandi.fr).

La réservation se fait en principe pour une période d'un an renouvelable. Le coût dépend du type de domaine. De 2 € / an pour un domaine finissant par .ovh jusqu'à 70 € / an pour un domaine finissant par .legal, en passant par un tarif de 14 € / an pour une extension .com.


> Attention : pour les avocats, le choix de votre domaine n'est pas libre.
L'article 10.5 du Règlement Intérieur National de la Profession d'Avocat dispose que :  
>
>*"Le nom de domaine doit comporter le nom de l'avocat ou la dénomination du cabinet en totalité ou en abrégé, qui peut être suivi ou précédé du mot « avocat ». L'utilisation de noms de domaine évoquant de façon générique le titre d'avocat ou un titre pouvant prêter à confusion, un domaine du droit ou une activité relevant de celles de l'avocat, est interdite."*
{.is-info}

Sur le site [OVH](www.ovh.com), la réservation d'un domaine se fait en cliquant sur [ce lien](https://www.ovh.com/fr/domaines/) : ![ovh_domain_1.png](/ovh_domain_1.png)

