---
title: Pré-Installation du serveur
description: Pré-installation
published: true
date: 2024-11-06T06:15:10.359Z
tags: 
editor: markdown
dateCreated: 2021-06-03T15:49:51.660Z
---

# Réservation d'un hébergement chez OVH CLOUD

Voici la démarche pour réserver un VPS, par exemple chez l'hébergeur OVH CLOUD :

1. Créez un compte sur https://www.ovh.com/auth/
2. Une fois l'inscription terminée, rendez vous ici pour réserver un VPS : https://www.ovh.com/fr/order/vps
3. Choisissez l'offre qui vous convient. Nous recommandons de démarrer avec l'offre VALUE qui est hébergée en France et intègre 40Go de stockage (sachant qu'on peut ajouter de l'espace disque plus tard en cas de besoin).
4. Sur la page suivante : "Configurez votre VPS", renseignez les champs comme suit :
 - Choix du système d'exploitation : Distribution Uniquement -> Debian -> Version 12
 - Localisation du datacenter : Europe de l'Ouest, France, Strasbourg (SBG)
 - Quantité : 1
5. Sur la page suivante : "Options", aucune option n'est indispensable mais la sauvegarde automatisée à 3€ / mois peut être judicieuse (possibilité de souscrire les options ultérieurement)
6. Sur la page suivante durée d'engagement, sélectionnez "sans engagement"
7. Sur la page suivante "Paiement de votre commande", choisissez le mode de paiement. La carte bancaire permet d'activer le serveur dans la journée. La mise en place d'un prélèvement reporte de 2 à 3 jours l'ouverture du serveur. Il est possible de changer le mode de paiement à tout moment par la suite.
8. Validez  les conditions générales en cochant les cases au bas de la page.
9. Finissez la commande en cliquant CONFIRMER ET PAYER

Vous recevrez, habituellement dans l'heure qui suit, un mail vous informant de l'ouverture du serveur, contenant le mot de passe d'accès.


# Installation sur un serveur physique

Ce tutoriel a été réalisé pour un mini PC [INTEL NUC8I5BEK2](https://www.amazon.fr/gp/product/B07JBM1CFH) équipé de [8Go de RAM](https://www.amazon.fr/Crucial-CT16G4SFD824A-PC4-19200-260-Pin-M%C3%A9moire/dp/B019FRD3SE/) et d'un [disque SSD NVMe de 500 Go](https://www.amazon.fr/Samsung-SSD-Interne-Plus-NVMe/dp/B07MBQPQ62/)

Une fois le NUC lancé, on commence par quelques réglages préalables :

1. Au démarrage du NUC, lorsque le logo INTEL apparait, tapez F2 pour entrer dans le menu du BIOS.
2. Cliquez sur "ADVANCED" pour entrer dans le menu avancé.
3. Dans la section "BOOT (UEFI Boot Priority)", décochez "UEFI BOOT" pour permettre au NUC de démarrer depuis son disque NVMe.
4. Dans la section "BOOT (Legacy Boot Priority)", déplacez le disque NVME en première position.
5. Dans la section "POWER", sélectionnez "AFTER POWER FAILURE : POWER ON" pour que le NUC redémarre en cas de coupure de courant.
6. Appuyez sur "F10" et "ENTREE" pour sauvegarder les modifications et redémarrer.

Il faut ensuite installer DEBIAN 12 sur le NUC.
1. télécharger l'ISO de DEBIAN 12 : https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-12.7.0-amd64-netinst.iso
2. installer l'image sur une clé USB d'au moins 1 Go avec par exemple le logiciel RUFUS : https://rufus.ie/fr_FR.html
3. DEBIAN ne reconnait pas la carte WIFI du NUC par défaut car elle requiert un pilote propriétaire INTEL. Ce pilote est téléchargeable ici : http://ftp.us.debian.org/debian/pool/non-free/f/firmware-nonfree/firmware-iwlwifi_20190114-2_all.deb. Il suffit de copier le fichier dans le dossier "firmware" de la clé USB pour que la carte wifi soit détectée pendant l'installation. Nous recommandons cependant de connecter le NUC en filaire et pas en WIFI. Dans ce cas cette étape est optionnelle.

Pour lancer l'installation il suffit ensuite d'insérer la clé USB dans un des ports USB du NUC et de le redémarrer. Il devrait lancer l'installation de DEBIAN 10.5 depuis la clé USB et vous devriez ensuite voir le menu d'installation bleu.

1. Choisissez "Install" (2e option) pour lancer l'installation non graphique.
Ensuite répondez comme suit aux questions posées :
2. Select a language : English
3. Select your location : other -> Europe -> France
4. Configure locales : en_US.UTF-8
5. Keymap to use : French
6. Configure the Network : eno1 Intel Corporation Device<
7. Root Password : Choisissez un mot de passe de 10 caractères minimum mélangeant chiffres, minuscules, majuscules et caractères spéciaux et surtout ne l'oubliez pas !
8. Set up users and Passwords : debian
9. Choose a password for the new users : Choisissez un mot de passe de 10 caractères minimum mélangeant chiffres, minuscules, majuscules et caractères spéciaux et surtout ne l'oubliez pas !
10. Partition Disk : Manual -> /dev/nvme0n1 -> Yes -> pri/log -> Create a new partition -> 20 GB -> Primary -> Beginning -> Bootable flag : on -> Done setting up the partition -> Finish partitioning and write changes to disk -> No -> Yes
11. Software selection : Uniquement "SSH Server" et "standard system utilities"
12. Configure the package manager : France -> deb.debian.org -> Continue -> No
13. Install the GRUB boot loader : Yes -> /dev/nvme0n1
14. Finish the installation : Retirez la clé USB puis "continue"

A l'issue de l'installation, si vous avez suivi ces instructions, le système consomme 12Go et le reste de l'espace disque est libre et non configuré. Cet espace libre sera monté sur /srv et servira à stocker les données. Il est recommandé de chiffrer cette partition /srv pour que les données soient inutilisables en cas de vol du NUC. Les scripts d'installation ALL SPARK réaliseront ces opérations à votre place.

