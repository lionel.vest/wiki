---
title: Mise à jour du serveur
description: Mise à jour
published: true
date: 2024-11-06T06:30:49.584Z
tags: 
editor: markdown
dateCreated: 2021-06-10T23:10:05.810Z
---

# Mise à jour du serveur

Le serveur OPTIMUS applique automatiquement les mises à jour de sécurité du système d'exploitation DEBIAN.

S'agissant de services OPTIMUS, vous serez notifié dans l'application quand des mises à jour sont disponibles. Il est recommandé de les installer systématiquement.