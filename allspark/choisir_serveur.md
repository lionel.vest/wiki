---
title: Choisir son serveur
description: Choisir son serveur
published: true
date: 2024-11-28T23:44:16.828Z
tags: 
editor: markdown
dateCreated: 2021-06-03T15:17:44.363Z
---

# Choisir son serveur

OPTIMUS fonctionne avec le système d'exploitation Linux Debian 12
(mais rassurez vous, vous pourrez continuer à utiliser Windows ou MacOS côté utilisateur !)

Pour vous doter d'un serveur OPTIMUS vous pouvez
- soit acheter un ordinateur que vous installerez à votre bureau
- soit réserver un serveur chez un hébergeur professionnel

# Serveur physique ou virtuel ?
 
Ces éléments sont à prendre en considération pour éclairer votre choix :
- un serveur installé à votre cabinet peut subir vol, incendie ou dégât des eaux, alors qu'un serveur hébergé est en principe situé dans un bâtiment sécurisé qui réduit grandement le risque.
- un serveur installé à votre cabinet peut tomber en panne et doit être réparé par vos soins, alors qu'un serveur hébergé en ligne est entretenu par l'hébergeur et peut être remplacé dans l'heure en cas de problème grave
- La stabilité de la connexion internet d'un serveur hébergé est souvent bien supérieure à ce que peut délivrer un fournisseur d'accès internet à votre bureau.

Installer un serveur à son cabinet implique d'avoir une connexion internet rapide en envoi, fiable, et constante. La fibre semble de rigueur.

Cela implique également que votre FAI (fournisseur d'accès internet) permette de faire de l'autohébergement, à savoir :
- qu'il vous garantisse une adresse IP fixe
- qu'il ne bloque pas les ports 25 et 587 qui permettent d'envoyer des mails
- qu'il permette de configurer un REVERSE DNS 
- que sa box permette de configurer un DNS LOOPBACK (sinon il faut manuellement configurer les fichiers hosts)

A notre connaissance, le seul FAI tout public qui offre ces options est FREE, à condition d'activer les options idoines sur son compte FREEBOX. Pour le reste, seules des offres professionnelles sont compatibles.

Avec un serveur hébergé, si vous n'avez plus de connexion internet au bureau, vous n'aurez plus accès à rien. Installer un serveur à son cabinet est le seul moyen pour pouvoir travailler "hors ligne". Toutefois, en cas de coupure, on peut aujourd'hui facilement basculer sur la 4G de son téléphone comme solution de secours provisoire.

Si vous travaillez sur des dossiers extrèmement sensibles, que vous disposez d'un service informatique fiable, et d'un bâtiment sécurisé, il est parfois plus judicieux d'héberger vous-même votre serveur. En effet, un serveur hébergé peut faire l'objet d'une réquisition ou d'une saisie sur demande d'un juge et l'hébergeur s'y pliera sans que vous en soyez préalablement informé. 

# Quel fournisseur ?

Nous n'avons aucun partenariat et vous laissons le libre choix de votre fournisseur.

Si vous optez pour un hébergement, veillez à choisir un hébergeur français qui respecte le droit européen et le RGPD. Evitez les serveurs mutualisés. Privilégiez les offres VPS (Virtual Private Server), Public Cloud ou encore les serveurs dédiés. Le serveur doit être doté à minima de 2Go de Ram et 20 Go d'espace disque. Le système occupant 12 Go d'espace disque, il est recommandé d'ajouter au moins 25 Go supplémentaires pour vos données.

Sachez que si vous choisissez l'hébergeur OVH, nous proposons, grâce à leur API d'automatisation, un processus simplifié d'installation via le site https://installer.optimus-avocats.fr

Si vous optez pour un serveur physique installé à votre cabinet, privilégiez une machine avec minimum 8Go de RAM et un disque SSD d'au moins 250 Go.

Si vous ne vous sentez pas capable de configurer un tel serveur par vous-même, n'hésitez pas à solliciter un prestataire informatique. Nos solutions étant libres et documentées, n'importe quel informaticien digne de ce nom devrait pouvoir vous installer la solution en consultant ce WIKI.

# Quel coût ?

L'acquisition ou la location du serveur est la seule dépense induite par notre solution.

Nous avons fait de nombreux efforts pour que nos logiciels consomment le moins de ressources possibles, ce qui permet de réduire le coût d'achat d'un serveur à environ 490 € HT, c'est à dire en dessous du seuil comptable qui vous oblige à immobiliser la dépense. Un tel serveur peut aisément accueillir une trentaine d'utilisateurs.

Si vous optez pour l'hébergement, le coût mensuel se situe entre 5€ HT / mois et 20€ € HT / mois selon l'espace disque dont vous aurez besoin. Comptez 10Go d'espace pour le système et au moins 20Go par utilisateur.

