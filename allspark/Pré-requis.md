---
title: Pré-requis
description: Pré-requis
published: true
date: 2024-03-03T07:42:34.420Z
tags: 
editor: markdown
dateCreated: 2021-06-09T22:15:46.973Z
---

# Pré-requis

OPTIMUS est une compilation de logiciels libres qui sont fournis totalement gratuitement.

Pour les utiliser, il vous faudra :
- un serveur, c'est à dire un ordinateur sur lequel les faire fonctionner, et qui stockera également vos données (fichiers, courriels, agendas, contacts, bases de données). Pour bien choisir votre serveur, rendez vous sur [cette page](/fr/allspark/choisir_serveur)
- un nom de domaine, c'est à dire une adresse du type moncabinet.com ou dupond-avocat.fr qui sera le point de connexion entre votre serveur et le reste du monde. Pour bien choisir votre nom de domaine, rendez vous sur [cette page](/fr/allspark/choisir-son-domaine)

Ces deux éléments sont nécessairement payants puisqu'ils ne sont pas fournis pas l'association CYBERTRON mais par des sociétés tierces.
Toutefois, nous avons oeuvré pour que notre solution consomme le minimum de ressources possibles, ce qui permet de réduire le coût au maximum. Pour exemple, un utilisateur voulant disposer de 40 Go d'espace disque peut trouver un prestataire pouvant proposer un hébergement moyennant un prix total inférieur à 5 € / mois.
